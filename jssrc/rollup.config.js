export default {
    entry: 'src/app.js',
    indent: '    ',
    // sourceMap: true,
    targets: [
        {
            format: 'umd',
            moduleName: 'HEH',
            dest: '../site/js/heh-app.js'
        },
    ]
}
