/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

function HEH_on_inital_app_load() {
    HEH.Remote().get_logged_in_user()
    .then(function(result){
        HEH.PanelSwitcher().resetAllAround();
        HEH.PanelSwitcher().showStartPanelForUser( result.user );
        HEH_show_top_card();
    });
}

function HEH_reset_all_around() {
    HEH.PanelSwitcher().resetAllAround();
}

function HEH_show_top_card() {
    $('#top-card').removeClass('d-none');
}
function HEH_hide_top_card() {
    $('#top-card').addClass('d-none');
}

function HEH_show_registration() {
    HEH.PanelSwitcher().showRegistration();
}
function HEH_show_login() {
    HEH.PanelSwitcher().showLogin();
}

function HEH_do_register() {
    HEH_reset_all_around();
    HEH.RegistrationForm().doRegister().then( HEH_show_login );
}
function HEH_do_login() {
    HEH_reset_all_around();
    HEH.LoginForm().doLogin().then( HEH_show_top_card );
}
function HEH_do_logout() {
    HEH_reset_all_around();
    HEH.LoginForm().doLogout().then( HEH_hide_top_card );
}

function HEH_members_add_meal() {
    HEH.MealsForm( 'member-meal-form', 'member-filter-form' ).addMeal( 'member-meals-table' );
};
function HEH_members_edit_meal() {
    HEH.MealsForm( 'member-meal-form', 'member-filter-form' ).updateMeal( 'member-meals-table' );
};
function HEH_members_list_meals() {
    HEH.MealsForm( 'member-meal-form', 'member-filter-form' ).listMeals( 'member-meals-table' );
};
function HEH_members_refresh_settings() {
    HEH.SettingsForm( 'user-settings-form' ).retrieveSettings();
};
function HEH_members_update_settings() {
    HEH.SettingsForm( 'user-settings-form' )
    .updateSettings()
    .then( HEH_members_list_meals );
};


function HEH_admin_add_meal() {
    let user_id = $('#admin-user-form #user-id').val();
    HEH.MealsForm( 'admin-meal-form', 'admin-filter-form', user_id ).addMeal( 'admin-meals-table' );
};
function HEH_admin_edit_meal() {
    let user_id = $('#admin-user-form #user-id').val();
    HEH.MealsForm( 'admin-meal-form', 'admin-filter-form', user_id ).updateMeal( 'admin-meals-table' );
};
function HEH_admin_list_meals() {
    let user_id = $('#admin-user-form #user-id').val();
    HEH.MealsForm( 'admin-meal-form', 'admin-filter-form', user_id ).listMeals( 'admin-meals-table' );
};


function HEH_manager_add_user() {
    HEH.UserForm( 'manager-user-form' ).addUser( 'manager-users-table' );
};
function HEH_manager_edit_user() {
    HEH.UserForm( 'manager-user-form' ).updateUser( 'manager-users-table' );
};
function HEH_manager_list_users() {
    HEH.UserForm( 'manager-user-form' ).listUsers( 'manager-users-table' );
};


function HEH_admin_add_user() {
    HEH.UserForm( 'admin-user-form' ).addUser( 'admin-users-table' );
};
function HEH_admin_edit_user() {
    HEH.UserForm( 'admin-user-form' ).updateUser( 'admin-users-table' );
};
function HEH_admin_list_users() {
    HEH.UserForm( 'admin-user-form' ).listUsers( 'admin-users-table' );
};
