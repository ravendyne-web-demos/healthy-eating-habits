/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { Remote } from './Remote';
import { PanelSwitcher } from './PanelSwitcher';
import { CONFIG } from "../config";
import { DOMTablePopulate } from '../util/DOMUtil';
import { MealsForm } from './MealsForm';


function UserForm( user_form_id, for_admin )
{
    //
    // Case when UserForm is called as a function
    //
    if( ! ( this instanceof UserForm ) )
    {
        return new UserForm( user_form_id, for_admin )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    if( user_form_id === undefined ) {
        throw 'Required parameter "user_form_id" is missing.';
    }
    let _user_form_id = user_form_id;
    // Admin user form view needs some more features so we flag that case here.
    // Not the best way of handling use cases for a class
    // but when there's a deadline looming, anything goes
    // as long as it's clear cut :)
    // A better way would be to i.e. have AdminUserForm() that extends UserForm()
    // and implements stuff specific to admin UI handling
    let _for_admin = for_admin;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        user_form_id:
        {
            get: function() { return _user_form_id },
        },
        for_admin:
        {
            get: function() { return _for_admin },
        },
    })
}

//
// PUBLIC CLASS METHODS
//
Object.assign( UserForm.prototype,
{
    populateTable: function( table_id, data_array ) {

        const self = this;

        DOMTablePopulate( table_id, data_array,
        // called for every <tr> element in the table
        function( tr_idx, data_el, dom_el ){
            // atatch user ID to <tr>
            dom_el.data('userId', data_el.id );
            dom_el.off( 'click' );
            dom_el.on( 'click', function(){
                UserForm( self.user_form_id ).setUserFormFields( data_el );
                if( self.for_admin ) {
                    // last-minute, ugly hack for admin use case
                    MealsForm( 'admin-meal-form', 'admin-filter-form', data_el.id ).listMeals( 'admin-meals-table' );
                    PanelSwitcher().enableAdminButtons();
                }
            });
        },
        // called for every <td> element in a row
        function( td_idx, data_el, dom_el ){
            switch( td_idx ) {
                case 0:
                    // delete button
                    dom_el.find('button[name="user-delete-button"]').off( 'click' );
                    dom_el.find('button[name="user-delete-button"]').on( 'click', function(){
                        UserForm( self.user_form_id ).deleteUser( data_el.id, table_id );
                    });
                break;
                case 1:
                    dom_el.html( data_el.username );
                break;
                case 2:
                    dom_el.html( data_el.email );
                break;
            }
        });
    },

    setUserFormFields: function( data ) {

        $(`#${this.user_form_id} #user-id`).val( data.id );
        $(`#${this.user_form_id} #user-username`).val( data.username );
        $(`#${this.user_form_id} #user-email`).val( data.email );
        if( data.manager ) {
            $(`#${this.user_form_id} #user-group`).val( '3' );
        } else {
            $(`#${this.user_form_id} #user-group`).val( '2' );
        }

        // clear password field
        $(`#${this.user_form_id} #user-password`).val();
    },

    getUserFormFields: function() {

        let user_id = $(`#${this.user_form_id} #user-id`).val();
        let user_username = $(`#${this.user_form_id} #user-username`).val();
        let user_email = $(`#${this.user_form_id} #user-email`).val();
        let user_password = $(`#${this.user_form_id} #user-password`).val();
        let user_group = $(`#${this.user_form_id} #user-group`).val();
        let is_manager = user_group == 3;

        let user_data = {};

        user_data['id'] = user_id || undefined;
        user_data['username'] = user_username || undefined;
        user_data['email'] = user_email || undefined;
        user_data['password'] = user_password || undefined;
        user_data['manager'] = is_manager;

        return user_data;
    },

    listUsers: function( table_id ) {

        const self = this;

        const dfd = $.Deferred();

        Remote().user_list( {} )
        .done(function( result )
        {
            if( result.success ) {

                self.populateTable( table_id, result.users );

                dfd.resolve( result );

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    deleteUser: function( user_id, table_id ) {

        const self = this;

        const dfd = $.Deferred();

        Remote().user_delete({ 'id': user_id })
        .done(function( result ){

            // refresh users table
            self.listUsers( table_id ).done(function(){
                dfd.resolve( result );
            });

        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    addUser: function( table_id ) {

        let user_data = this.getUserFormFields();

        const self = this;

        const dfd = $.Deferred()

        Remote().user_create( user_data )
        .done(function( result )
        {
            // console.log(result);
            if( result.success ) {

                // refresh users table
                self.listUsers( table_id ).done(function(){
                    dfd.resolve( result );
                });

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    updateUser: function( table_id ) {

        let user_data = this.getUserFormFields();

        const self = this;

        const dfd = $.Deferred()

        Remote().user_update( user_data )
        .done(function( result )
        {
            if( result.success ) {

                // refresh users table
                self.listUsers( table_id ).done(function(){
                    dfd.resolve( result );
                });

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    toString: function()
    {
    },
} )

export { UserForm }
