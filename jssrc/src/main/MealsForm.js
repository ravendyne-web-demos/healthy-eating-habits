/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { Remote } from './Remote.js'
import { PanelSwitcher } from './PanelSwitcher.js'
import { CONFIG } from "../config";
import { DOMTablePopulate } from '../util/DOMUtil';
import { DateLocalToISO, TimeLocalToISO, DateISOToLocal, TimeISOToLocal } from '../util/GeneralUtil';


function MealsForm( meal_form_id, filter_form_id, user_id )
{
    //
    // Case when MealsForm is called as a function
    //
    if( ! ( this instanceof MealsForm ) )
    {
        return new MealsForm( meal_form_id, filter_form_id, user_id )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    if( meal_form_id === undefined ) {
        throw 'Required parameter "meal_form_id" is missing.';
    }
    if( filter_form_id === undefined ) {
        throw 'Required parameter "filter_form_id" is missing.';
    }
    let _meal_form_id = meal_form_id;
    let _filter_form_id = filter_form_id;
    let _user_id = user_id;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        meal_form_id:
        {
            get: function() { return _meal_form_id },
        },
        filter_form_id:
        {
            get: function() { return _filter_form_id },
        },
        user_id:
        {
            get: function() { return _user_id },
        },
    })
}

//
// PUBLIC CLASS METHODS
//
Object.assign( MealsForm.prototype,
{
    populateTable: function( table_id, data_array ) {

        const self = this;

        DOMTablePopulate( table_id, data_array,
        // called for every <tr> element in the table
        function( tr_idx, data_el, dom_el ){
            // atatch meal ID to <tr>
            dom_el.data('mealId', data_el.id );
            dom_el.off( 'click' );
            dom_el.on( 'click', function(){
                MealsForm( self.meal_form_id, self.filter_form_id, self.user_id ).setMealFormFields( data_el );
            });
            if( data_el.exceeded ) {
                dom_el.addClass('table-danger');
            } else {
                dom_el.addClass('table-success');
            }
        },
        // called for every <td> element in a row
        function( td_idx, data_el, dom_el ){
            switch( td_idx ) {
                case 0:
                    // delete button
                    dom_el.find('button').off( 'click' );
                    dom_el.find('button').on( 'click', function(){
                        MealsForm( self.meal_form_id, self.filter_form_id, self.user_id ).deleteMeal( data_el.id, table_id );
                    });
                break;
                case 1:
                    dom_el.html( DateISOToLocal( data_el.mdate ) );
                break;
                case 2:
                    dom_el.html( TimeISOToLocal( data_el.mtime ) );
                break;
                case 3:
                    dom_el.html( data_el.name );
                break;
                case 4:
                    dom_el.html( data_el.calories );
                break;
            }
        });
    },

    setMealFormFields: function( data ) {

        $(`#${this.meal_form_id} #meal-id`).val( data.id );
        $(`#${this.meal_form_id} #meal-date`).val( DateISOToLocal( data.mdate ) );
        $(`#${this.meal_form_id} #meal-time`).val( TimeISOToLocal( data.mtime ) );
        $(`#${this.meal_form_id} #meal-name`).val( data.name );
        $(`#${this.meal_form_id} #meal-calories`).val( data.calories );
    },

    getMealFormFields: function() {

        let meal_id = $(`#${this.meal_form_id} #meal-id`).val();
        let meal_date = $(`#${this.meal_form_id} #meal-date`).val();
        let meal_time = $(`#${this.meal_form_id} #meal-time`).val();
        let meal_name = $(`#${this.meal_form_id} #meal-name`).val();
        let meal_calories = $(`#${this.meal_form_id} #meal-calories`).val();

        meal_date = DateLocalToISO( meal_date );
        meal_time = TimeLocalToISO( meal_time );

        return {
            'id' : meal_id,
            'mdate' : meal_date,
            'mtime' : meal_time,
            'name' : meal_name,
            'calories' : meal_calories
        };
    },

    getMealFilterFields: function() {

        let filter = {};

        // populate filter options, if any
        let meal_date_from = $(`#${this.filter_form_id} #filter-date-from`).val();
        let meal_date_to = $(`#${this.filter_form_id} #filter-date-to`).val();
        let meal_time_from = $(`#${this.filter_form_id} #filter-time-from`).val();
        let meal_time_to = $(`#${this.filter_form_id} #filter-time-to`).val();

        if( meal_date_from ) {
            meal_date_from = DateLocalToISO( meal_date_from );
            filter['date_from'] = meal_date_from;
        }
        if( meal_date_to ) {
            meal_date_to = DateLocalToISO( meal_date_to );
            filter['date_to'] = meal_date_to;
        }
        if( meal_time_from ) {
            meal_time_from = TimeLocalToISO( meal_time_from );
            filter['time_from'] = meal_time_from;
        }
        if( meal_time_to ) {
            meal_time_to = TimeLocalToISO( meal_time_to );
            filter['time_to'] = meal_time_to;
        }

        return filter;
    },

    listMeals: function( table_id ) {

        const self = this;

        let filter = this.getMealFilterFields();

        let parameters = { "filter" : filter }
        if( self.user_id !== undefined ) {
            parameters.user_id = self.user_id;
        }

        const dfd = $.Deferred();

        Remote().meal_retrieve( parameters )
        .done(function( result )
        {
            if( result.success ) {

                self.populateTable( table_id, result.meals );

                dfd.resolve( result );

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    deleteMeal: function( meal_id, table_id ) {

        const self = this;

        const dfd = $.Deferred();

        Remote().meal_delete({ 'id': meal_id })
        .done(function( result ){

            // refresh meals table
            self.listMeals( table_id ).done(function(){
                dfd.resolve( result );
            });

        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    addMeal: function( table_id ) {

        let meal_data = this.getMealFormFields();

        const self = this;

        if( self.user_id !== undefined ) {
            meal_data.user_id = self.user_id;
        }

        const dfd = $.Deferred()

        Remote().meal_create( meal_data )
        .done(function( result )
        {
            // console.log(result);
            if( result.success ) {

                // refresh meals table
                self.listMeals( table_id ).done(function(){
                    dfd.resolve( result );
                });

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    updateMeal: function( table_id ) {

        let meal_data = this.getMealFormFields();

        const self = this;

        const dfd = $.Deferred()

        Remote().meal_update( meal_data )
        .done(function( result )
        {
            if( result.success ) {

                // refresh meals table
                self.listMeals( table_id ).done(function(){
                    dfd.resolve( result );
                });

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    toString: function()
    {
    },
} )

export { MealsForm }
