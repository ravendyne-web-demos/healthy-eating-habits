/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { Remote } from './Remote'
import { PanelSwitcher } from './PanelSwitcher'
import { CONFIG } from '../config';


function LoginForm( form_id )
{
    //
    // Case when LoginForm is called as a function
    //
    if( ! ( this instanceof LoginForm ) )
    {
        return new LoginForm( form_id )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    let _form_id = form_id || CONFIG.LOGIN_FORM_ID;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        form_id:
        {
            get: function() { return _form_id },
        },
    })
}

//
// PUBLIC CLASS METHODS
//
Object.assign( LoginForm.prototype,
{
    doLogin: function() {

        const dfd = $.Deferred()

        let identity = $(`#${this.form_id} #login-identity`).val();
        let password = $(`#${this.form_id} #login-password`).val();
        let remember_me = $(`#${this.form_id} #login-remember`).prop('checked');

        PanelSwitcher().showLoading();

        Remote().login( identity, password, remember_me )
        .done(function( result )
        {
            if( result.success ) {

                PanelSwitcher().showStartPanelForUser( result.user );
                dfd.resolve(result)

            } else {

                PanelSwitcher().showLogin();
                PanelSwitcher().showMessage( result.message );
                dfd.reject()
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject()
        })

        return dfd.promise()
    },

    doLogout: function() {

        const dfd = $.Deferred()

        Remote().logout()
        .then(result => {
            PanelSwitcher().showLogin();
            dfd.resolve(result)
        })
        .fail(jqxhr => {
            PanelSwitcher().showLogin();
            dfd.reject()
        })

        return dfd.promise()
    },

    toString: function()
    {
    },
} )

export { LoginForm }
