/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { CONFIG } from '../config';
import { MealsForm } from './MealsForm';
import { UserForm } from './UserForm';
import { SettingsForm } from './SettingsForm';
import { DOMTableClear } from '../util/DOMUtil';


function PanelSwitcher( tabs_list_id )
{
    //
    // Case when PanelSwitcher is called as a function
    //
    if( ! ( this instanceof PanelSwitcher ) )
    {
        return new PanelSwitcher( tabs_list_id )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    let _tabs_list_id = tabs_list_id || CONFIG.TABS_LIST_ID;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
        {
            tabs_list_id:
            {
                get: function() { return _tabs_list_id },
            },
        })
    }

//
// PUBLIC CLASS METHODS
//
Object.assign( PanelSwitcher.prototype,
{
    showRegistration: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_REGISTER_ID}"]`).tab('show');
    },

    showLogin: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_LOGIN_ID}"]`).tab('show');
    },

    showMembers: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_MEMBERS_ID}"]`).tab('show');
    },

    showManager: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_MANAGER_ID}"]`).tab('show');
    },

    showAdmin: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_ADMIN_ID}"]`).tab('show');
    },

    showLoading: function() {
        $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_LOADING_ID}"]`).tab('show');
    },

    disableAdminButtons: function() {
        $('#admin-meal-form button[name="meal-add-button"]').prop( 'disabled', true );
        $('#admin-meal-form button[name="meal-update-button"]').prop( 'disabled', true );
    },

    enableAdminButtons: function() {
        $('#admin-meal-form button[name="meal-add-button"]').removeProp( 'disabled' );
        $('#admin-meal-form button[name="meal-add-button"]').removeAttr( 'disabled' );
        $('#admin-meal-form button[name="meal-update-button"]').removeProp( 'disabled' );
        $('#admin-meal-form button[name="meal-update-button"]').removeAttr( 'disabled' );
    },

    resetAllAround: function() {

        DOMTableClear( 'member-meals-table' );
        DOMTableClear( 'admin-meals-table' );
        DOMTableClear( 'admin-users-table' );
        DOMTableClear( 'manager-users-table' );
    
        $('#member-meal-form').each(function(){
            this.reset();
        });
        $('#member-meal-form #meal-id').removeAttr('value');
    
        $('#admin-meal-form').each(function(){
            this.reset();
        });
        $('#admin-meal-form #meal-id').removeAttr('value');
    
        $('#member-filter-form').each(function(){
            this.reset();
        });
        $('#admin-filter-form').each(function(){
            this.reset();
        });
    
        $('#manager-user-form').each(function(){
            this.reset();
        });
        $('#manager-user-form #user-id').removeAttr('value');
    
        $('#admin-user-form').each(function(){
            this.reset();
        });
        $('#admin-user-form #user-id').removeAttr('value');

        this.disableAdminButtons();
    },

    showStartPanelForUser: function( user_data ) {

        if( user_data.groups.includes( 'admin' ) ) {

            SettingsForm( 'user-settings-form' ).hideForm();
            this.showAdmin();
            // true -> admin hack, see UserForm.for_admin for description
            UserForm( 'admin-user-form', true ).listUsers( 'admin-users-table' );

        } else if( user_data.groups.includes( 'managers' ) ) {

            SettingsForm( 'user-settings-form' ).hideForm();
            this.showManager();
            UserForm( 'manager-user-form' ).listUsers( 'manager-users-table' );

        } else if( user_data.groups.includes( 'members' ) ) {

            let settings_form = SettingsForm( 'user-settings-form' );
            settings_form.showForm();
            settings_form.retrieveSettings();
            this.showMembers();
            MealsForm( 'member-meal-form', 'member-filter-form' ).listMeals( 'member-meals-table' );

        } else {
            SettingsForm( 'user-settings-form' ).hideForm();
            // TODO show message 'unsupported group??'
            this.showMessage( `User "${user_data.username}" has no assigned application page` );
        }
    },

    showMessage: function( message ) {
        $(`#${CONFIG.SYSTEM_MODAL_ID} #message`).html(message);
        $(`#${CONFIG.SYSTEM_MODAL_ID}`).modal('show');
        console.error( message );
    },

    toString: function()
    {
    },
} )

export { PanelSwitcher }
