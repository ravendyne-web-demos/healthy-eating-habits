/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { Remote } from './Remote'
import { PanelSwitcher } from './PanelSwitcher'
import { CONFIG } from '../config';


function SettingsForm( form_id )
{
    //
    // Case when SettingsForm is called as a function
    //
    if( ! ( this instanceof SettingsForm ) )
    {
        return new SettingsForm( form_id )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    let _form_id = form_id || CONFIG.USER_SETTINGS_FORM_ID;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        form_id:
        {
            get: function() { return _form_id },
        },
    })
}

//
// PUBLIC CLASS METHODS
//
Object.assign( SettingsForm.prototype,
{

    showForm: function() {

        $(`#${this.form_id}`).removeClass( 'd-none' );
    },

    hideForm: function() {

        $(`#${this.form_id}`).addClass( 'd-none' );
    },

    setFormFields: function( data ) {

        $(`#${this.form_id} #settings-calories`).val( data.calories || '' );
    },

    getFormFields: function() {

        let settings_calories = $(`#${this.form_id} #settings-calories`).val();

        let settings_data = {};

        settings_data['calories'] = settings_calories || undefined;

        return settings_data;
    },

    updateSettings: function() {

        let settings_data = this.getFormFields();

        const dfd = $.Deferred()

        Remote().settings_update( settings_data )
        .done(function( result )
        {
            if( result.success ) {

                dfd.resolve( result );

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    retrieveSettings: function() {

        const self = this;

        const dfd = $.Deferred()

        Remote().settings_retrieve( {} )
        .done(function( result )
        {
            if( result.success ) {

                let settings_data = result.data;
                self.setFormFields( settings_data );

                dfd.resolve( result );

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    toString: function()
    {
    },
} )

export { SettingsForm }
