/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { Remote } from './Remote'
import { PanelSwitcher } from './PanelSwitcher'
import { CONFIG } from '../config';


function RegistrationForm( form_id )
{
    //
    // Case when RegistrationForm is called as a function
    //
    if( ! ( this instanceof RegistrationForm ) )
    {
        return new RegistrationForm( form_id )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    let _form_id = form_id || CONFIG.REGISTRATION_FORM_ID;

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        form_id:
        {
            get: function() { return _form_id },
        },
    })
}

//
// PUBLIC CLASS METHODS
//
Object.assign( RegistrationForm.prototype,
{

    clearForm: function() {

        $(`#${this.form_id} #registration-username`).val( '' );
        $(`#${this.form_id} #registration-email`).val( '' );
        $(`#${this.form_id} #registration-password`).val( '' );
        $(`#${this.form_id} #registration-password-repeat`).val( '' );
    },

    getFormFields: function() {

        let username = $(`#${this.form_id} #registration-username`).val();
        let email = $(`#${this.form_id} #registration-email`).val();
        let password = $(`#${this.form_id} #registration-password`).val();
        let password_repeat = $(`#${this.form_id} #registration-password-repeat`).val();

        let registration_data = {};

        registration_data['username'] = username || undefined;
        registration_data['email'] = email || undefined;
        registration_data['password'] = password || undefined;
        registration_data['password_repeat'] = password_repeat || undefined;

        return registration_data;
    },

    doRegister: function() {

        const self = this;

        let registration_data = this.getFormFields();

        const dfd = $.Deferred()

        if( registration_data.password != registration_data.password_repeat ) {
            PanelSwitcher().showMessage( 'Passwords must match' );
            dfd.reject();
            return;
        }

        Remote().register( registration_data )
        .done(function( result )
        {
            if( result.success ) {

                self.clearForm();
                dfd.resolve( result );

            } else {

                PanelSwitcher().showMessage( result.message );
                dfd.reject();
            }
        })
        .fail(function( xhr )
        {
            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject();
        })

        return dfd.promise();
    },

    toString: function()
    {
    },
} )

export { RegistrationForm }
