/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

import { PanelSwitcher } from './PanelSwitcher.js'


//
// read-only properties in javascript
// ¯\_(ツ)_/¯
//

// defined here, these will be initialized only once, on load
let _AUTH = {}
Object.defineProperties(_AUTH,
{
    LOGIN:
    {
        get: function() { return '/api/index.php?/auth/login' },
    },
    LOGOUT:
    {
        get: function() { return '/api/index.php?/auth/logout' },
    },
    REGISTER:
    {
        get: function() { return '/api/index.php?/auth/register' },
    },
    WHO_AM_I:
    {
        get: function() { return '/api/index.php?/auth/who_am_i' },
    },
})

let _MEALS = {}
Object.defineProperties(_MEALS,
{
    CREATE:
    {
        get: function() { return '/api/index.php?/meals/create' },
    },
    RETRIEVE:
    {
        get: function() { return '/api/index.php?/meals/retrieve' },
    },
    UPDATE:
    {
        get: function() { return '/api/index.php?/meals/update' },
    },
    DELETE:
    {
        get: function() { return '/api/index.php?/meals/delete' },
    },
})

let _USER = {}
Object.defineProperties(_USER,
{
    CREATE:
    {
        get: function() { return '/api/index.php?/user/create' },
    },
    RETRIEVE:
    {
        get: function() { return '/api/index.php?/user/retrieve' },
    },
    UPDATE:
    {
        get: function() { return '/api/index.php?/user/update' },
    },
    DELETE:
    {
        get: function() { return '/api/index.php?/user/delete' },
    },
    LIST:
    {
        get: function() { return '/api/index.php?/user/list' },
    },
})

let _SETTINGS = {}
Object.defineProperties(_SETTINGS,
{
    RETRIEVE:
    {
        get: function() { return '/api/index.php?/settings/retrieve' },
    },
    UPDATE:
    {
        get: function() { return '/api/index.php?/settings/update' },
    },
})

let _API_URL = {}
Object.defineProperties(_API_URL,
{
    AUTH:
    {
        get: function() { return _AUTH },
    },
    MEALS:
    {
        get: function() { return _MEALS },
    },
    USER:
    {
        get: function() { return _USER },
    },
    SETTINGS:
    {
        get: function() { return _SETTINGS },
    },
})

//
// REMOTE
//
function Remote(param) {
    //
    // Case when Remote is called as a function
    //
    if (!(this instanceof Remote)) {
        return new Remote(param)
    }

    Object.defineProperties(this,
    {
        API_URL:
        {
            get: function() { return _API_URL },
        },
    })
}


//
// PUBLIC CLASS METHODS
//
Object.assign(Remote.prototype,
{
    //
    // AUTH
    //
    login: function( identity, password, remember_me = false ) {
        return this.api_request( this.API_URL.AUTH.LOGIN, {
            "identity": identity,
            "password" : password,
            "remember_me" : remember_me
        });
    },

    logout: function() {
        return this.api_request( this.API_URL.AUTH.LOGOUT, {} );
    },

    register: function( data ) {
        return this.api_request( this.API_URL.AUTH.REGISTER, data );
    },

    get_logged_in_user: function() {
        return this.api_request_handler( this.API_URL.AUTH.WHO_AM_I, {} );
    },

    //
    // MEALS
    //
    meal_create: function( data ) {
        return this.api_request_handler( this.API_URL.MEALS.CREATE, data );
    },

    meal_retrieve: function( data ) {
        return this.api_request_handler( this.API_URL.MEALS.RETRIEVE, data );
    },

    meal_update: function( data ) {
        return this.api_request_handler( this.API_URL.MEALS.UPDATE, data );
    },

    meal_delete: function( data ) {
        return this.api_request_handler( this.API_URL.MEALS.DELETE, data );
    },

    //
    // USERS
    //
    user_create: function( data ) {
        return this.api_request_handler( this.API_URL.USER.CREATE, data );
    },

    user_retrieve: function( data ) {
        return this.api_request_handler( this.API_URL.USER.RETRIEVE, data );
    },

    user_list: function( data ) {
        return this.api_request_handler( this.API_URL.USER.LIST, data );
    },

    user_update: function( data ) {
        return this.api_request_handler( this.API_URL.USER.UPDATE, data );
    },

    user_delete: function( data ) {
        return this.api_request_handler( this.API_URL.USER.DELETE, data );
    },

    //
    // SETTINGS
    //
    settings_retrieve: function( data ) {
        return this.api_request_handler( this.API_URL.SETTINGS.RETRIEVE, data );
    },

    settings_update: function( data ) {
        return this.api_request_handler( this.API_URL.SETTINGS.UPDATE, data );
    },

    //
    // OTHER STUFF
    //
    api_request_handler: function( url, data ) {

        const self = this;
        const dfd = $.Deferred();

        this.api_request( url, data )
        .done( function( result ) {

            if( ! result.success && result.needs_login ) {
    
                PanelSwitcher().showLogin();
    
            } else {
    
                dfd.resolve( result );
            }
        })
        .fail( function( xhr ) {

            PanelSwitcher().showMessage( xhr.responseText );
            dfd.reject( xhr );
        });

        return dfd.promise();
    },

    api_request: function( url, data ) {
        return $.ajax({
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            processData: false,
            type: 'POST',
            url: url
        });
    },

    toString: function() {
    },
})

export { Remote }
