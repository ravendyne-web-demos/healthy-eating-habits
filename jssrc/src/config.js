/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var CONFIG = {

    TABS_LIST_ID: 'list-tabs',

    //
    // panels
    //
    PANEL_LOGIN_ID: 'panel-login',
    PANEL_MEMBERS_ID: 'panel-members',
    PANEL_MANAGER_ID: 'panel-manager',
    PANEL_ADMIN_ID: 'panel-admin',
    PANEL_LOADING_ID: 'panel-loading',
    PANEL_REGISTER_ID: 'panel-register',

    //
    // modal
    //
    SYSTEM_MODAL_ID: 'system-modal',

    //
    // forms
    //
    LOGIN_FORM_ID: 'login-form',
    REGISTRATION_FORM_ID: 'registration-form',
    MEMBER_MEAL_FORM_ID: 'member-meal-form',
    MEMBER_FILTER_FORM_ID: 'member-filter-form',
    USER_SETTINGS_FORM_ID: 'user-settings-form',
}

export { CONFIG }
