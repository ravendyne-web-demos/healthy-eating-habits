/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

const DateLocalToISO = function( date_string ) {
    return moment( date_string, 'L' ).format( 'YYYY-MM-DD' );
}
const TimeLocalToISO = function( time_string ) {
    return moment( time_string, 'LT' ).format( 'HH:mm:ss' );
}
const DateISOToLocal = function( date_string ) {
    return moment( date_string, 'YYYY-MM-DD' ).format( 'L' );
}
const TimeISOToLocal = function( time_string ) {
    return moment( time_string, 'HH:mm:ss' ).format( 'LT' );
}

export { DateLocalToISO, TimeLocalToISO, DateISOToLocal, TimeISOToLocal }
