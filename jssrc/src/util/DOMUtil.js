/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
/**
 * @author Ravendyne Inc / https://ravendyne.com
 */

 /**
  * Removes all rows from `<table>` with given ID.
  * 
  * If the table has `<tr>` with a class set to `template-row`, that row will not be removed.
  * 
  * @param {string} table_id 
  */
 const DOMTableClear = function( table_id ) {

    var template_row = $(`#${table_id} tr.template-row`).detach();
    $(`#${table_id} tbody`).empty().append( template_row );                
}

/**
 * Returns a **clone** of the `tr` in this table that is marked as template row.
 * Class `d-none` (Bootstrap `display: none;` helper) is removed from it.
 * 
 * Template rows are marked as such by assigning them `template-row` CSS class.
 * 
 * @param {string} table_id 
 */
const DOMTableTemplateRowClone = function( table_id ) {

    let template_row_clone = $(`#${table_id} tr.template-row`).first().clone();
    template_row_clone.removeClass('d-none');
    template_row_clone.removeClass('template-row');

    return template_row_clone;
}

const DOMTablePopulate = function( table_id, data_array, row_callback, column_callback ) {

    DOMTableClear( table_id );

    // clones template row element
    var template_row = DOMTableTemplateRowClone( table_id );
    var table_body = $(`#${table_id} tbody`);

    let row_number = 1;
    data_array.forEach(element => {

        let new_row = template_row.clone();

        row_callback( row_number, element, new_row );

        new_row.find('th').html( row_number );
        new_row.find('td').each( function( idx, el ){
            column_callback( idx, element, $(el) );
        });
        table_body.append( new_row );

        row_number++;
    });

    // discard cloned row element
    template_row.remove();
}

export { DOMTableClear, DOMTableTemplateRowClone, DOMTablePopulate }
