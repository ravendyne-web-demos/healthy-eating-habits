--
-- Database: `food_site`
--
USE `food_site`;

-- --------------------------------------------------------

--
-- View `meal_totals`
--

DROP VIEW IF EXISTS `meal_totals`;
CREATE VIEW `meal_totals` (
  `user_id`,
  `mdate`,
  `daily_total`
) AS
SELECT `user_id`, `mdate`, SUM(`calories`)
FROM `meals`
GROUP BY `user_id`, `mdate`;

