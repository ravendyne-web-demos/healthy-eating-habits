<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


class Mock_settings_service {

    private $params;

    public function __construct( $params ) {
        $this->params = $params;
    }

    public function get_for_user( $request ) {
        return [
            'success' => true,
            'data' => [
                'calories' => 1000,
            ]
        ];
    }
}