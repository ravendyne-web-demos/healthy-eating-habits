<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


function array_to_object( $array, $object = null ) {
    if( $object == null ) $object = new class {};
    foreach( $array as $key => $value) {
        $object->{$key} = $value;
    }
    return $object;
}

class OneRow {

    public function __construct( $row ) {
        array_to_object( $row, $this );
    }

    public function row() {
        return $this;
    }
}

class MultipleRows {

    private $rows;

    public function __construct( $rows ) {
        $this->rows = array_map( function($el) { return array_to_object( $el ); }, $rows );
    }

    public function result() {
        return $this->rows;
    }
}

class Mock_ion_auth {

    var $_params;
    var $_users;
    var $_groups;
    var $_current_user;

    var $_next_user_id;

    public function __construct( $params ) {

        $this->_params = $params;
        $this->_current_user = null;

        $this->_users = array_key_exists( 'users', $params ) ? $params['users'] : [];
        $this->_groups = array_key_exists( 'groups', $params ) ? $params['groups'] : [];

        $this->_next_user_id = 10000;
    }

    public function logged_in() {
        return $this->_params['logged_in'];
    }

    public function is_admin() {
        return $this->_params['is_admin'];;
    }

    private function get_user( $identity, $key = 'identity' ) {

        for( $idx = 0; $idx < count( $this->_users ); $idx++ ) {
            $user = $this->_users[ $idx ];
            if( $user[$key] === $identity ) {
                $user = array_merge( $user, [ '_idx' => $idx ] );
                return $user;
            }
        }

        return false;
    }

    public function login( $identity, $password, $remember = false ) {

        $user = $this->get_user( $identity );
        if( ! $user ) return false;

        if( $user['password'] !== $password )
            return false;

        $this->_current_user = $user;

        return true;
    }

    public function user( $user_id = null ) {

        $result = new OneRow([]);

        if( $user_id === null && $this->_current_user !== null ) {

            $result = new OneRow( $this->_current_user );

        } else {

            $user = $this->get_user( $user_id, 'user_id' );

            if( $user !== false ) {

                $result = new OneRow( $user );
            }
        }

        return $result;
    }

    public function logout() {

        $this->_current_user = null;
    }

    public function username_check( $username ) {

        $user = $this->get_user( $username, 'username' );

        if( ! $user ) return false;

        return true;        
    }

    public function email_check( $email ) {

        $user = $this->get_user( $email, 'email' );

        if( ! $user ) return false;

        return true;        
    }

    private function check_groups( $groups ) {

        foreach( $groups as $user_group ) {
            if( ! in_array( $user_group, $this->_groups ) ) return false;
        }

        return true;
    }

    public function register( $username, $password, $email, $additional_data, $groups ) {

        if( $this->username_check( $username ) ) return false;
        if( $this->email_check( $email ) ) return false;
        if( ! $this->check_groups( $groups ) ) return false;

        $user_id = $this->_next_user_id;
        $this->_next_user_id ++;

        $this->_users[] = [
            'user_id' => $user_id,
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'groups' => $groups,
        ];

        return $user_id;
    }

    public function update( $user_id, $user_data ) {

        $user = $this->get_user( $user_id, 'user_id' );
        if( $user === false ) return false;

        if( array_key_exists( 'groups', $user_data ) ) {
            $groups = $user_data['groups'];
            if( ! $this->check_groups( $groups ) ) return false;
        }

        $idx = $user['_idx'];

        if( array_key_exists( 'username', $user_data ) ) {
            $this->_users[ $idx ]['username'] = $user_data['username'];
        }

        if( array_key_exists( 'password', $user_data ) ) {
            $this->_users[ $idx ]['password'] = $user_data['password'];
        }

        if( array_key_exists( 'email', $user_data ) ) {
            $this->_users[ $idx ]['email'] = $user_data['email'];
        }

        if( array_key_exists( 'groups', $user_data ) ) {
            $this->_users[ $idx ]['groups'] = $user_data['groups'];
        }

        return true;
    }

    public function users( $group_id ) {

        $users = array_filter( $this->_users, function( $user ) use($group_id) {
            return in_array( $group_id, $user['groups'] );
        });

        return new MultipleRows( $users );
    }

    public function get_users_groups( $user_id ) {

        $user = $this->get_user( $user_id, 'user_id' );
        if( $user === false ) return new MultipleRows([]);

        return new MultipleRows( array_map( function($gn) { return ['name' => $gn]; }, $user['groups'] ) );
    }

    public function in_group( $group_id, $user_id ) {

        $user = $this->get_user( $user_id, 'user_id' );
        if( $user === false ) return false;

        return in_array( $group_id, $user['groups'] );
    }

    public function add_to_group( $group_id, $user_id ) {

        $user = $this->get_user( $user_id, 'user_id' );
        if( $user === false ) return false;
        if( ! $this->check_groups([ $group_id ]) ) return false;

        $idx = $user['_idx'];
        $this->_users[ $idx ]['groups'][] = $group_id;

        return true;
    }

    public function remove_from_group( $group_id, $user_id ) {

        $user = $this->get_user( $user_id, 'user_id' );
        if( $user === false ) return false;
        if( ! $this->check_groups([ $group_id ]) ) return false;

        $idx = $user['_idx'];
        $new_groups = array_filter( $this->_users[ $idx ]['groups'], function($val) use($group_id) {
            return $val != $group_id;
        });
        $this->_users[ $idx ]['groups'] = $new_groups;

        return true;
    }

    public function errors() {
        return array( 'dummy' );
    }
}
