<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


class MockQuery {
    var $data;
    public function __construct( $result ) {
        $this->data = $result;
    }
    public function result() {
        return $this->data;
    }
    public function result_array() {
        return $this->data;
    }
}

class Mock_db {

    var $_params;
    var $_affected_rows;

    public $last_insert;
    public $last_update;
    public $last_select;
    public $and_where_conditions;

    public function __construct( $params ) {

        $this->_params = $params;
        $this->last_insert = [];
        $this->last_update = [];
        $this->last_select = '';
        $this->and_where_conditions = [];
        $this->_affected_rows = array_key_exists( 'affected_rows', $params ) ? $params['affected_rows'] : 1;
    }

    public function where( $col, $cond = [] ) {
        if( is_array( $col ) ) {
            $this->and_where_conditions = array_merge( $this->and_where_conditions, $col );
        } else {
            $this->and_where_conditions[$col] = $cond;
        }
    }

    public function update( $table, $row ) {
        $this->last_update = $row;
        return true;
    }

    public function insert( $table, $row ) {
        $this->last_insert = $row;
        return true;
    }

    public function select( $cols ) {
        $this->last_select = $cols;
    }

    public function get( $table ) {
        return new MockQuery([]);
    }

    public function delete( $table ) {
        return true;
    }

    public function reset_query() {
    }

    public function affected_rows() {
        return $this->_affected_rows;
    }
}
