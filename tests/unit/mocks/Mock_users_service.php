<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


class Mock_users_service {

    private $users;

    public function __construct( $users ) {
        $this->users = $users;
    }

    private function get_user( $user_id ) {

        foreach( $this->users as $user ) {
            if( $user['user_id'] === $user_id ) return $user;
        }

        return false;
    }

    public function is_member( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        $user = $this->get_user( $id );

        return $user['is_member'];
    }

    public function is_manager( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        $user = $this->get_user( $id );

        return $user['is_manager'];
    }

    public function retrieve_user( $request ) {

        $user = $this->get_user( $request['id'] );

        if( $user === false ) {
            return array( 'success' => false );
        }

        return array( 'success' => true, 'user' => $user );
    }
}