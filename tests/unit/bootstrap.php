<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

// So CodeIgniter doesn't barf
define('BASEPATH', '');

//
// PATHS
//
$API_LIBRARIES_PATH = dirname( __FILE__ ) . '/../../api/application/libraries';
$MOCKS_PATH = dirname( __FILE__ ) . '/mocks';



//
// MOCKS
//
require_once( dirname( __FILE__ ) . '/constants.php' );
require_once( $MOCKS_PATH . '/Mock_ion_auth.php' );
require_once( $MOCKS_PATH . '/Mock_users_service.php' );
require_once( $MOCKS_PATH . '/Mock_settings_service.php' );
require_once( $MOCKS_PATH . '/Mock_db.php' );

//
// CUT-s
//
require_once( $API_LIBRARIES_PATH . '/Site_auth_service.php' );
require_once( $API_LIBRARIES_PATH . '/Site_meals_service.php' );
require_once( $API_LIBRARIES_PATH . '/Site_users_service.php' );
// require_once( $API_LIBRARIES_PATH . '/Site_settings_service.php' );
