<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class MealsCreateTest extends TestCase
{

    public function testCreateMealMember(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->create_meal( [ 'mdate' => '2019-06-14', 'mtime' => '12:59:59', 'name' => 'my meal', 'calories' => '1000' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'data', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->last_insert ) );
        $this->assertEquals( $sms->db->last_insert['user_id'], 22 );
    }

    public function testCreateMealAdmin(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => true,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_ADMIN_ID] ],
                [ 'user_id' => 33, 'identity' => 'c', 'username' => 'c', 'password' => 'd', 'email' => 'c@d.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22 && is_admin


        $response = $sms->create_meal( [ 'user_id' => 33, 'mdate' => '2019-06-14', 'mtime' => '12:59:59', 'name' => 'my meal', 'calories' => '1000' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'data', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->last_insert ) );
        $this->assertEquals( $sms->db->last_insert['user_id'], 33 );
    }

    public function testCreateMealMemberTryAsOtherUser(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
                [ 'user_id' => 33, 'identity' => 'c', 'username' => 'c', 'password' => 'd', 'email' => 'c@d.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->create_meal( [ 'user_id' => 33, 'mdate' => '2019-06-14', 'mtime' => '12:59:59', 'name' => 'my meal', 'calories' => '1000' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'data', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->last_insert ) );
        $this->assertEquals( $sms->db->last_insert['user_id'], 22 );
    }

}
