<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class MealsRetrieveTest extends TestCase
{

    public function testRetrieveMealMemberNoFilter(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->site_settings_service = new Mock_settings_service([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->retrieve_meals( [] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'meals', $response ) );
        $this->assertTrue( array_key_exists( 'filter', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->and_where_conditions ) );
        $this->assertEquals( $sms->db->and_where_conditions['user_id'], 22 );

        $this->assertFalse( array_key_exists( 'mdate >=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'mdate <=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'mtime >=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'mtime <=', $sms->db->and_where_conditions ) );
    }

    public function testRetrieveMealMemberWithFilter(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->site_settings_service = new Mock_settings_service([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->retrieve_meals( [ 'filter' => [
            'date_from' => '2019-06-11',
            'date_to' => '2019-06-12',
            'time_from' => '08:00:00',
            'time_to' => '14:00:00',
            'fake_filter_condition' => 'fake',
        ] ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'meals', $response ) );
        $this->assertTrue( array_key_exists( 'filter', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->and_where_conditions ) );
        $this->assertEquals( $sms->db->and_where_conditions['user_id'], 22 );

        $this->assertTrue( array_key_exists( 'mdate >=', $sms->db->and_where_conditions ) );
        $this->assertTrue( array_key_exists( 'mdate <=', $sms->db->and_where_conditions ) );
        $this->assertTrue( array_key_exists( 'mtime >=', $sms->db->and_where_conditions ) );
        $this->assertTrue( array_key_exists( 'mtime <=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'fake_filter_condition', $sms->db->and_where_conditions ) );
    }

    public function testRetrieveMealMemberWithPartialFilter(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->site_settings_service = new Mock_settings_service([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->retrieve_meals( [ 'filter' => [
            'date_from' => '2019-06-11',
            'time_from' => '08:00:00',
        ] ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'meals', $response ) );
        $this->assertTrue( array_key_exists( 'filter', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->and_where_conditions ) );
        $this->assertEquals( $sms->db->and_where_conditions['user_id'], 22 );

        $this->assertTrue( array_key_exists( 'mdate >=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'mdate <=', $sms->db->and_where_conditions ) );
        $this->assertTrue( array_key_exists( 'mtime >=', $sms->db->and_where_conditions ) );
        $this->assertFalse( array_key_exists( 'mtime <=', $sms->db->and_where_conditions ) );
    }

    public function testRetrieveMealAdmin(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => true,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_ADMIN_ID] ],
                [ 'user_id' => 33, 'identity' => 'c', 'username' => 'c', 'password' => 'd', 'email' => 'c@d.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->site_settings_service = new Mock_settings_service([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22 && is_admin


        $response = $sms->retrieve_meals( [ 'user_id' => 33 ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'meals', $response ) );
        $this->assertTrue( array_key_exists( 'filter', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->and_where_conditions ) );
        $this->assertEquals( $sms->db->and_where_conditions['user_id'], 33 );
    }

    public function testRetrieveMealMemberTryAsOtherUser(): void
    {
        $sms = new Site_meals_service();

        $sms->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
                [ 'user_id' => 33, 'identity' => 'c', 'username' => 'c', 'password' => 'd', 'email' => 'c@d.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);
        $sms->db = new Mock_db([]);
        $sms->site_settings_service = new Mock_settings_service([]);
        $sms->ion_auth->login( 'a', 'b' ); // user_id == 22


        $response = $sms->retrieve_meals( [ 'user_id' => 33 ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'meals', $response ) );
        $this->assertTrue( array_key_exists( 'filter', $response ) );

        $this->assertTrue( array_key_exists( 'user_id', $sms->db->and_where_conditions ) );
        $this->assertEquals( $sms->db->and_where_conditions['user_id'], 22 );
    }

}
