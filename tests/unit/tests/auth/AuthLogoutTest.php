<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class AuthLogoutTest extends TestCase
{

    public function testLogout(): void
    {
        $sas = new Site_auth_service();

        $sas->ion_auth = new Mock_ion_auth([ 'logged_in' => false, 'is_admin' => false ]);


        $response = $sas->logout( [] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
    }
}
