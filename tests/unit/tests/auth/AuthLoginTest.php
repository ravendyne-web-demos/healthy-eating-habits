<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class AuthLoginTest extends TestCase
{

    public function testLoginEmpty(): void
    {
        $sas = new Site_auth_service();

        $sas->ion_auth = new Mock_ion_auth([ 'logged_in' => false, 'is_admin' => false ]);


        $response = $sas->login( [] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }

    public function testLoginNonExisting(): void
    {
        $sas = new Site_auth_service();

        $sas->ion_auth = new Mock_ion_auth([
            'logged_in' => false,
            'is_admin' => false,
            'users' => [],
        ]);


        $response = $sas->login( [ 'identity' => 'a', 'password' => 'b' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }

    public function testLoginRequiredParams(): void
    {
        $sas = new Site_auth_service();

        $sas->ion_auth = new Mock_ion_auth([
            'logged_in' => false,
            'is_admin' => false,
            'users' => [
                [ 'identity' => 'a', 'password' => 'b' ],
            ],
        ]);


        $response = $sas->login( [ 'identity' => 'a' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );

        $response = $sas->login( [ 'password' => 'b' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }

    public function testLogin(): void
    {
        $sas = new Site_auth_service();

        $users = [
            [ 'user_id' => 22, 'identity' => 'a', 'password' => 'b' ],
        ];
        $sas->ion_auth = new Mock_ion_auth([
            'logged_in' => false,
            'is_admin' => false,
            'users' => $users,
        ]);
        $sas->site_users_service = new Mock_users_service( $users );


        $response = $sas->login( [ 'identity' => 'a', 'password' => 'b' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'user', $response ) );


        $response = $sas->login( [ 'identity' => 'aa', 'password' => 'b' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sas->login( [ 'identity' => 'a', 'password' => 'bb' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }
}
