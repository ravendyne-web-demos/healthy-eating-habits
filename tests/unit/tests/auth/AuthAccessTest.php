<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class AuthAccessTest extends TestCase
{
    public function testNotLoggedIn(): void
    {
        $sas = new Site_auth_service();

        $sas->ion_auth = new Mock_ion_auth([ 'logged_in' => false, 'is_admin' => false ]);

        $this->assertFalse( $sas->check_access('meals') );
        $this->assertFalse( $sas->check_access('users') );
        $this->assertFalse( $sas->check_access('settings') );

        $sas->ion_auth = new Mock_ion_auth([ 'logged_in' => false, 'is_admin' => true ]);

        $this->assertFalse( $sas->check_access('meals') );
        $this->assertFalse( $sas->check_access('users') );
        $this->assertFalse( $sas->check_access('settings') );
    }

    public function testAdminAccess(): void
    {
        $sas = new Site_auth_service();
        $sas->ion_auth = new Mock_ion_auth([ 'logged_in' => true, 'is_admin' => true ]);

        $this->assertTrue( $sas->check_access('meals') );
        $this->assertTrue( $sas->check_access('users') );
        $this->assertTrue( $sas->check_access('settings') );
    }

    public function testMemberAccess(): void
    {
        $users = [
            [ 'user_id' => 22, 'identity' => 'a', 'password' => 'b', 'is_member' => true, 'is_manager' => false ],
        ];

        $ia = new Mock_ion_auth([ 'logged_in' => true, 'is_admin' => false, 'users' => $users ]);
        $sus = new Mock_users_service($users);
        $sus->ion_auth = $ia;

        $sas = new Site_auth_service();
        $sas->site_users_service = $sus;
        $sas->ion_auth = $ia;
        $sas->login( [ 'identity' => 'a', 'password' => 'b' ] );

        // TODO add groups
        $this->assertTrue( $sas->check_access('meals') );
        $this->assertFalse( $sas->check_access('users') );
        $this->assertTrue( $sas->check_access('settings') );
    }

    public function testManagerAccess(): void
    {
        $users = [
            [ 'user_id' => 22, 'identity' => 'a', 'password' => 'b', 'is_member' => false, 'is_manager' => true ],
        ];

        $ia = new Mock_ion_auth([ 'logged_in' => true, 'is_admin' => false, 'users' => $users ]);
        $sus = new Mock_users_service($users);
        $sus->ion_auth = $ia;

        $sas = new Site_auth_service();
        $sas->site_users_service = $sus;
        $sas->ion_auth = $ia;
        $sas->login( [ 'identity' => 'a', 'password' => 'b' ] );

        // TODO add groups
        $this->assertFalse( $sas->check_access('meals') );
        $this->assertTrue( $sas->check_access('users') );
        $this->assertTrue( $sas->check_access('settings') );
    }
}
