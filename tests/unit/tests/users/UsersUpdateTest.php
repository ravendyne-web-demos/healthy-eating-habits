<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class UsersUpdateTest extends TestCase
{

    public function testUpdate(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID], 'active' => '1' ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);

        $user = $sus->ion_auth->user( 22 )->row();
        $this->assertEquals( $user->username, 'a' );
        $this->assertEquals( $user->password, 'b' );
        $this->assertEquals( $user->email, 'a@b.com' );

        $response = $sus->update_site_user( [ 'id' => 22, 'username' => 'aaa', 'password' => 'bbb', 'email' => 'aaa@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );

        $user = $sus->ion_auth->user( 22 )->row();
        $this->assertEquals( $user->username, 'aaa' );
        $this->assertEquals( $user->password, 'bbb' );
        $this->assertEquals( $user->email, 'aaa@b.com' );
    }

    public function testUpdateToManager(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID], 'active' => '1' ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);


        $response = $sus->update_site_user( [ 'id' => 22, 'manager' => true ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );

        $user = $sus->ion_auth->user( 22 )->row();
        $this->assertTrue( in_array( GROUPS_MANAGER_ID, $user->groups ) );
        $this->assertFalse( in_array( GROUPS_MEMBER_ID, $user->groups ) );
    }

    public function testUpdateToMember(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MANAGER_ID], 'active' => '1' ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);


        $response = $sus->update_site_user( [ 'id' => 22, 'manager' => false ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );

        $user = $sus->ion_auth->user( 22 )->row();
        $this->assertTrue( in_array( GROUPS_MEMBER_ID, $user->groups ) );
        $this->assertFalse( in_array( GROUPS_MANAGER_ID, $user->groups ) );
    }
}
