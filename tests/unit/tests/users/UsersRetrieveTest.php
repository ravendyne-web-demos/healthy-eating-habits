<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class UsersRetrieveTest extends TestCase
{

    public function testRetrieveMalformed(): void
    {
        $sus = new Site_users_service();

        $response = $sus->retrieve_user([]);

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }

    public function testRetrieve(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);

        $response = $sus->retrieve_user( [ 'id' => 22 ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'user', $response ) );
        $this->assertTrue( is_array( $response['user'] ) );
    }

    public function testRetrieveNonExistant(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);

        $response = $sus->retrieve_user( [ 'id' => 33 ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );
    }
}
