<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class UsersRetrieveListTest extends TestCase
{

    public function testRetrieveList(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => false,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'mem', 'username' => 'mem', 'password' => 'b', 'email' => 'mem@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MEMBER_ID], 'active' => '1' ],
                [ 'user_id' => 33, 'identity' => 'mgr', 'username' => 'mgr', 'password' => 'b', 'email' => 'mgr@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MANAGER_ID], 'active' => '1' ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);

        $response = $sus->retrieve_user_list([]);

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'users', $response ) );
        $this->assertEquals( count( $response['users'] ), 1 );
    }

    public function testAdminRetrieveList(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'is_admin' => true,
            'users' => [
                [ 'user_id' => 22, 'identity' => 'mem', 'username' => 'mem', 'password' => 'b', 'email' => 'mem@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MEMBER_ID], 'active' => '1' ],
                [ 'user_id' => 33, 'identity' => 'mgr', 'username' => 'mgr', 'password' => 'b', 'email' => 'mgr@b.com', 'first_name' => '', 'last_name' => '', 'groups' => [GROUPS_MANAGER_ID], 'active' => '1' ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);

        $response = $sus->retrieve_user_list([]);

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'users', $response ) );
        $this->assertEquals( count( $response['users'] ), 2 );
    }
}
