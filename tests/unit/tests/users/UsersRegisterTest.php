<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class UsersRegisterTest extends TestCase
{

    public function testRegisterMember(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'logged_in' => false,
            'is_admin' => false,
            'users' => [
                [ 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);


        $response = $sus->register_site_member( [ 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_member( [ 'username' => 'new_a', 'password' => 'b', 'email' => 'a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_member( [ 'username' => 'a', 'password' => 'b', 'email' => 'new_a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_member( [ 'username' => 'new_a', 'password' => 'b', 'email' => 'new_a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'user_id', $response ) );
    }

    public function testRegisterManager(): void
    {
        $sus = new Site_users_service();

        $sus->ion_auth = new Mock_ion_auth([
            'logged_in' => false,
            'is_admin' => false,
            'users' => [
                [ 'identity' => 'a', 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com', 'groups' => [GROUPS_MEMBER_ID] ],
            ],
            'groups' => [
                GROUPS_MANAGER_ID,
                GROUPS_MEMBER_ID,
            ]
        ]);


        $response = $sus->register_site_manager( [ 'username' => 'a', 'password' => 'b', 'email' => 'a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_manager( [ 'username' => 'new_a', 'password' => 'b', 'email' => 'a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_manager( [ 'username' => 'a', 'password' => 'b', 'email' => 'new_a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertFalse( $response['success'] );
        $this->assertTrue( array_key_exists( 'message', $response ) );


        $response = $sus->register_site_manager( [ 'username' => 'new_a', 'password' => 'b', 'email' => 'new_a@b.com' ] );

        $this->assertTrue( array_key_exists( 'success', $response ) );
        $this->assertTrue( $response['success'] );
        $this->assertTrue( array_key_exists( 'user_id', $response ) );
    }
}
