## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from selenium.webdriver.common.by import By

class AppFormLocators(object):
    MEMBER_MEAL_FORM = (By.ID, 'member-meal-form')
    MEMBER_MEAL_FORM_XPATH = "//form[@id='member-meal-form']"
    MEMBER_MEAL_TABLE = (By.ID, 'member-meals-table')
    MEMBER_MEAL_TABLE_XPATH = "//table[@id='member-meals-table']"

    MEMBER_MEAL_FILTER_FORM = (By.ID, 'member-filter-form')
    MEMBER_MEAL_FILTER_FORM_XPATH = "//form[@id='member-filter-form']"

    MANAGER_USER_FORM = (By.ID, 'manager-user-form')
    MANAGER_USER_FORM_XPATH = "//form[@id='manager-user-form']"
    MANAGER_USER_TABLE = (By.ID, 'manager-users-table')
    MANAGER_USER_TABLE_XPATH = "//table[@id='manager-users-table']"

    ADMIN_USER_FORM = (By.ID, 'admin-user-form')
    ADMIN_USER_FORM_XPATH = "//form[@id='admin-user-form']"
    ADMIN_USER_TABLE = (By.ID, 'admin-users-table')
    ADMIN_USER_TABLE_XPATH = "//table[@id='admin-users-table']"

    ADMIN_MEAL_FORM = (By.ID, 'admin-meal-form')
    ADMIN_MEAL_FORM_XPATH = "//form[@id='admin-meal-form']"
    ADMIN_MEAL_TABLE = (By.ID, 'admin-meals-table')
    ADMIN_MEAL_TABLE_XPATH = "//table[@id='admin-meals-table']"

class ToolbarLocators(object):
    LOGOUT_BUTTON = (By.NAME, 'logout-button')

class LoginPageLocators(object):
    IDENTITY_FIELD = (By.ID, 'login-identity')
    PASSWORD_FIELD = (By.ID, 'login-password')
    LOGIN_BUTTON = (By.NAME, 'login-button')

class MealsFormLocators(object):
    DATE_FIELD = (By.ID, 'meal-date')
    DATE_FIELD_XPATH = "//input[@id='meal-date']"
    TIME_FIELD = (By.ID, 'meal-time')
    TIME_FIELD_XPATH = "//input[@id='meal-time']"
    NAME_FIELD = (By.ID, 'meal-name')
    NAME_FIELD_XPATH = "//input[@id='meal-name']"
    CALORIES_FIELD = (By.ID, 'meal-calories')
    CALORIES_FIELD_XPATH = "//input[@id='meal-calories']"

    ADD_BUTTON = (By.NAME, 'meal-add-button')
    ADD_BUTTON_XPATH = "//button[@name='meal-add-button']"
    UPDATE_BUTTON = (By.NAME, 'meal-update-button')
    UPDATE_BUTTON_XPATH = "//button[@name='meal-update-button']"
    RESET_BUTTON = (By.NAME, 'meal-reset-button')
    RESET_BUTTON_XPATH = "//button[@name='meal-reset-button']"

class MealsTableLocators(object):
    DELETE_ROW_BUTTON = (By.NAME, 'meal-delete-button')
    DELETE_ROW_BUTTON_XPATH = "//button[@name='meal-delete-button']"


class MealsFilterFormLocators(object):
    DATE_FROM_FIELD = (By.ID, 'filter-date-from')
    DATE_TO_FIELD = (By.ID, 'filter-date-to')
    TIME_FROM_FIELD = (By.ID, 'filter-time-from')
    TIME_TO_FIELD = (By.ID, 'filter-time-to')

    FILTER_BUTTON = (By.NAME, 'filter-filter-button')
    FILTER_BUTTON_XPATH = "//button[@name='filter-filter-button']"
    RESET_BUTTON = (By.NAME, 'filter-reset-button')
    RESET_BUTTON_XPATH = "//button[@name='filter-reset-button']"

class UsersFormLocators(object):
    GROUP_FIELD = (By.ID, 'user-group')
    USERNAME_FIELD = (By.ID, 'user-username')
    EMAIL_FIELD = (By.ID, 'user-email')
    PASSWORD_FIELD = (By.ID, 'user-password')

    ADD_BUTTON = (By.NAME, 'user-add-button')
    ADD_BUTTON_XPATH = "//button[@name='user-add-button']"
    UPDATE_BUTTON = (By.NAME, 'user-update-button')
    UPDATE_BUTTON_XPATH = "//button[@name='user-update-button']"
    RESET_BUTTON = (By.NAME, 'user-reset-button')
    RESET_BUTTON_XPATH = "//button[@name='user-reset-button']"

class UsersTableLocators(object):
    DELETE_ROW_BUTTON = (By.NAME, 'user-delete-button')
    DELETE_ROW_BUTTON_XPATH = "//button[@name='user-delete-button']"
