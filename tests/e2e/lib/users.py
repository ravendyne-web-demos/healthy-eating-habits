## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass, DataTable
from locators import AppFormLocators
from locators import UsersFormLocators, UsersTableLocators

from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.ui import Select

#
# Base class(es) for other classes
#

class UsersForm(LibBaseClass):

    def locator(self):
        return ()

    def get_form(self):
        return self.driver.find_element( *self.locator() )

    def get_field_values(self):
        elem = self.get_form().find_element( *UsersFormLocators.USERNAME_FIELD )
        username = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *UsersFormLocators.EMAIL_FIELD )
        email = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *UsersFormLocators.PASSWORD_FIELD )
        password = elem.get_attribute( 'value' )

        return (username, email, password)

    def fill_form(self, username, email, password):
        if username:
            elem = self.get_form().find_element( *UsersFormLocators.USERNAME_FIELD )
            elem.clear()
            elem.send_keys( username )

        if email:
            elem = self.get_form().find_element( *UsersFormLocators.EMAIL_FIELD )
            elem.clear()
            elem.send_keys( email )

        if password:
            elem = self.get_form().find_element( *UsersFormLocators.PASSWORD_FIELD )
            elem.clear()
            elem.send_keys( password )

    def do_add(self):
        elem = self.get_form().find_element( *UsersFormLocators.ADD_BUTTON )
        elem.click()

    def do_update(self):
        elem = self.get_form().find_element( *UsersFormLocators.UPDATE_BUTTON )
        elem.click()

    def do_reset(self):
        elem = self.get_form().find_element( *UsersFormLocators.RESET_BUTTON )
        elem.click()


class UsersTable(DataTable):

    def locator(self):
        return AppFormLocators.MANAGER_USER_TABLE

    def find_row_delete_button(self, elem):
        return elem.find_element( *UsersTableLocators.DELETE_ROW_BUTTON )


#
# Classes for specific forms and tables
#

class AdminUsersForm(UsersForm):

    def locator(self):
        return AppFormLocators.ADMIN_USER_FORM

    def visible(self):
        elem_group = self.get_form().find_element( *UsersFormLocators.GROUP_FIELD )
        elem_username = self.get_form().find_element( *UsersFormLocators.USERNAME_FIELD )
        elem_email = self.get_form().find_element( *UsersFormLocators.EMAIL_FIELD )
        elem_password = self.get_form().find_element( *UsersFormLocators.PASSWORD_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_username )( driver ) and \
                expected.visibility_of( elem_email )( driver ) and \
                expected.visibility_of( elem_password )( driver ) and \
                expected.visibility_of( elem_group )( driver )
        return cond

    def get_field_values(self):
        username, email, password = super(AdminUsersForm, self).get_field_values()

        elem = self.get_form().find_element( *UsersFormLocators.GROUP_FIELD )
        group = elem.get_attribute( 'value' )

        return (username, email, password, group)

    def fill_form(self, username, email, password, group):
        super(AdminUsersForm, self).fill_form( username, email, password )

        if group:
            elem = self.get_form().find_element( *UsersFormLocators.GROUP_FIELD )
            select = Select( elem )
            select.select_by_value( group )


class AdminUsersTable(UsersTable):

    def locator(self):
        return AppFormLocators.ADMIN_USER_TABLE




class ManagerUsersForm(UsersForm):

    def locator(self):
        return AppFormLocators.MANAGER_USER_FORM

    def visible(self):
        elem_username = self.get_form().find_element( *UsersFormLocators.USERNAME_FIELD )
        elem_email = self.get_form().find_element( *UsersFormLocators.EMAIL_FIELD )
        elem_password = self.get_form().find_element( *UsersFormLocators.PASSWORD_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_username )( driver ) and \
                expected.visibility_of( elem_email )( driver ) and \
                expected.visibility_of( elem_password )( driver )
        return cond


class ManagerUsersTable(UsersTable):

    def locator(self):
        return AppFormLocators.MANAGER_USER_TABLE
