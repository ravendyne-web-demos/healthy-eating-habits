## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass
from locators import ToolbarLocators

from selenium.webdriver.support import expected_conditions as expected



class Toolbar(LibBaseClass):

    def visible(self):
        def cond( driver ):
            return \
                expected.visibility_of_element_located( ToolbarLocators.LOGOUT_BUTTON )
        return cond

    def do_logout(self):
        driver = self.driver

        elem = driver.find_element( *ToolbarLocators.LOGOUT_BUTTON )
        elem.click()

