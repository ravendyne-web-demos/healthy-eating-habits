## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from selenium.webdriver.support import expected_conditions as expected

class expected_not(object):
    """ An expectation of given condition NOT having been fulfilled, i.e.
        `expected_not( selenium.webdriver.support.expected_conditions.text_to_be_present_in_element( locator, text ) )`
    """
    def __init__(self, cond):
        self.cond = cond

    def __call__(self, driver):
        return not self.cond( driver )
