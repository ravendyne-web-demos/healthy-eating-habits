## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from selenium.webdriver.support import expected_conditions as expected

from test_tools import expected_not


class LibBaseClass(object):

    def __init__(self, driver):
        self.driver = driver


class DataTable(LibBaseClass):

    def locator(self):
        return ()

    def visible(self):
        def cond( driver ):
            return \
                expected.visibility_of_element_located( self.locator() )( driver )
        return cond

    def get_table(self):
        return self.driver.find_element( *self.locator() )

    def contains_row_with_text(self, text):
        return self._contains_row_with_text_condition( text )( self.driver )

    def doesnt_contain_row_with_text(self, text):
        return self._doesnt_contain_row_with_text_condition( text )( self.driver )

    def expected_to_contain_row_with_text(self, text):
        return self._contains_row_with_text_condition( text )

    def expected_to_not_contain_row_with_text(self, text):
        return self._doesnt_contain_row_with_text_condition( text )

    def _contains_row_with_text_condition(self, text):
        return expected.text_to_be_present_in_element( self.locator(), text )

    def _doesnt_contain_row_with_text_condition(self, text):
        return expected_not( expected.text_to_be_present_in_element( self.locator(), text ) )

    def find_row_with_text(self, text):
        return self.get_table().find_element_by_xpath( ".//tr[.//text()[contains(., '" + text + "')]]" )
    
    def click_row(self, elem):
        driver = self.driver

        # https://bugzilla.mozilla.org/show_bug.cgi?id=1374283
        # elem.click()
        driver.execute_script("arguments[0].click()", elem)
