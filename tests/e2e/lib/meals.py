## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass, DataTable
from locators import AppFormLocators
from locators import MealsFormLocators, MealsFilterFormLocators
from locators import MealsTableLocators

from selenium.webdriver.support import expected_conditions as expected


#
# Base class(es) for other classes
#

class MealsForm(LibBaseClass):

    def locator(self):
        return ()

    def get_form(self):
        return self.driver.find_element( *self.locator() )

    def visible(self):
        elem_date = self.get_form().find_element( *MealsFormLocators.DATE_FIELD )
        elem_time = self.get_form().find_element( *MealsFormLocators.TIME_FIELD )
        elem_name = self.get_form().find_element( *MealsFormLocators.NAME_FIELD )
        elem_calories = self.get_form().find_element( *MealsFormLocators.CALORIES_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_date )( driver ) and \
                expected.visibility_of( elem_time )( driver ) and \
                expected.visibility_of( elem_name )( driver ) and \
                expected.visibility_of( elem_calories )( driver )
        return cond

    def get_field_values(self):
        elem = self.get_form().find_element( *MealsFormLocators.DATE_FIELD )
        meal_date = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFormLocators.TIME_FIELD )
        meal_time = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFormLocators.NAME_FIELD )
        name = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFormLocators.CALORIES_FIELD )
        calories = elem.get_attribute( 'value' )

        return (meal_date, meal_time, name, calories)

    def fill_form(self, meal_date, meal_time, name, calories):
        if meal_date:
            elem = self.get_form().find_element( *MealsFormLocators.DATE_FIELD )
            elem.clear()
            elem.send_keys( meal_date )

        if meal_time:
            elem = self.get_form().find_element( *MealsFormLocators.TIME_FIELD )
            elem.clear()
            elem.send_keys( meal_time )

        if name:
            elem = self.get_form().find_element( *MealsFormLocators.NAME_FIELD )
            elem.clear()
            elem.send_keys( name )

        if calories:
            elem = self.get_form().find_element( *MealsFormLocators.CALORIES_FIELD )
            elem.clear()
            elem.send_keys( calories )

    def do_add(self):
        elem = self.get_form().find_element( *MealsFormLocators.ADD_BUTTON )
        elem.click()

    def do_update(self):
        elem = self.get_form().find_element( *MealsFormLocators.UPDATE_BUTTON )
        elem.click()

    def do_reset(self):
        elem = self.get_form().find_element( *MealsFormLocators.RESET_BUTTON )
        elem.click()

class MealsFilterForm(LibBaseClass):

    def locator(self):
        return ()

    def get_form(self):
        return self.driver.find_element( *self.locator() )

    def visible(self):
        elem_date_from = self.get_form().find_element( *MealsFilterFormLocators.DATE_FROM_FIELD )
        elem_date_to = self.get_form().find_element( *MealsFilterFormLocators.DATE_TO_FIELD )
        elem_time_from = self.get_form().find_element( *MealsFilterFormLocators.TIME_FROM_FIELD )
        elem_time_to = self.get_form().find_element( *MealsFilterFormLocators.TIME_TO_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_date_from )( driver ) and \
                expected.visibility_of( elem_date_to )( driver ) and \
                expected.visibility_of( elem_time_from )( driver ) and \
                expected.visibility_of( elem_time_to )( driver )
        return cond

    def get_field_values(self):
        elem = self.get_form().find_element( *MealsFilterFormLocators.DATE_FROM_FIELD )
        date_from = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFilterFormLocators.DATE_TO_FIELD )
        date_to = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFilterFormLocators.TIME_FROM_FIELD )
        time_from = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *MealsFilterFormLocators.TIME_TO_FIELD )
        time_to = elem.get_attribute( 'value' )

        return (date_from, date_to, time_from, time_to)

    def fill_form(self, date_from, date_to, time_from, time_to):
        if date_from:
            elem = self.get_form().find_element( *MealsFilterFormLocators.DATE_FROM_FIELD )
            elem.clear()
            elem.send_keys( date_from )

        if date_to:
            elem = self.get_form().find_element( *MealsFilterFormLocators.DATE_TO_FIELD )
            elem.clear()
            elem.send_keys( date_to )

        if time_from:
            elem = self.get_form().find_element( *MealsFilterFormLocators.TIME_FROM_FIELD )
            elem.clear()
            elem.send_keys( time_from )

        if time_to:
            elem = self.get_form().find_element( *MealsFilterFormLocators.TIME_TO_FIELD )
            elem.clear()
            elem.send_keys( time_to )

    def do_filter(self):
        elem = self.get_form().find_element( *MealsFilterFormLocators.FILTER_BUTTON )
        elem.click()

    def do_reset(self):
        elem = self.get_form().find_element( *MealsFilterFormLocators.RESET_BUTTON )
        elem.click()


class MealsTable(DataTable):

    # def contains_meal_with_name(self, name):
    #     return self.contains_row_with_text_condition( name )( self.driver )

    # def doesnt_contain_meal_with_name(self, name):
    #     return self.doesnt_contain_row_with_text_condition( name )( self.driver )

    # def expected_to_contain_meal_with_name(self, name):
    #     return self.contains_row_with_text_condition( name )

    # def expected_to_not_contain_meal_with_name(self, name):
    #     return self.doesnt_contain_row_with_text_condition( name )

    def find_row_delete_button(self, elem):
        return elem.find_element( *MealsTableLocators.DELETE_ROW_BUTTON )


#
# Classes for specific forms and tables
#

class MemberMealsForm(MealsForm):

    def locator(self):
        return AppFormLocators.MEMBER_MEAL_FORM

class AdminMealsForm(MealsForm):

    def locator(self):
        return AppFormLocators.ADMIN_MEAL_FORM

class MemberMealsFilterForm(MealsFilterForm):

    def locator(self):
        return AppFormLocators.MEMBER_MEAL_FILTER_FORM

# class AdminMealsFilterForm(MealsFilterForm):

#     def locator(self):
#         return AppFormLocators.ADMIN_MEAL_FILTER_FORM

class MemberMealsTable(MealsTable):

    def locator(self):
        return AppFormLocators.MEMBER_MEAL_TABLE


class AdminMealsTable(MealsTable):

    def locator(self):
        return AppFormLocators.ADMIN_MEAL_TABLE

