## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

from lib_base import LibBaseClass
from locators import LoginPageLocators

from login_page import LoginPage
from toolbar import Toolbar

from selenium.webdriver.support.ui import WebDriverWait

from datetime import date as ddate, time as dtime
import mysql.connector


class AppHelper(LibBaseClass):

    def login(self, identity, password):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )
        wait.until( login_page.visible() )

        login_page.fill_form( identity, password )
        login_page.do_login()

    def logout(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()

def LocalDateString( year, month, day ):
    a_date = ddate( year, month, day )
    return a_date.strftime('%x')

def LocalTimeString( hour, minue, second ):
    a_time = dtime( hour, minue, second )
    return a_time.strftime('%X')

def ISODateString( year, month, day ):
    a_date = ddate( year, month, day )
    return a_date.strftime('%Y-%m-%d')

def ISOTimeString( hour, minue, second ):
    a_time = dtime( hour, minue, second )
    return a_time.strftime('%H:%M:%S')





def _do_bunch_of_sql( cursor, sql_stmts ):
    sql_cmds = sql_stmts.split(';')
    for one_sql_cmd in sql_cmds:
        try:
            if one_sql_cmd.strip() != '':
                cursor.execute(one_sql_cmd)
        except IOError, msg:
            print "Error executing a command SQL: ", msg

def RunSQLFile( conn, file_path ):

    sql_stmts = ''

    with open(file_path) as f:
        sql_stmts = f.read()

    dbcursor = conn.cursor()

    sql_cmds = sql_stmts.split(';')

    for one_sql_cmd in sql_cmds:

        if one_sql_cmd.strip() != '':
            # print one_sql_cmd
            dbcursor.execute( one_sql_cmd )

    dbcursor.close()


def DBResetData():
    conn = mysql.connector.connect(
        database = cfg.APP_DB_DATABASE,
        host = cfg.APP_DB_HOST,
        user = cfg.APP_DB_USER,
        passwd = cfg.APP_DB_PASS
    )
    dbcursor = conn.cursor()

    dbcursor.execute('TRUNCATE TABLE `meals`')

    dbcursor.close()
    conn.close()

def DBResetUsers():
    conn = mysql.connector.connect(
        database = cfg.APP_DB_DATABASE,
        host = cfg.APP_DB_HOST,
        user = cfg.APP_DB_USER,
        passwd = cfg.APP_DB_PASS
    )

    RunSQLFile( conn, cfg.APP_DB_USERS_SQL_FILE )

    conn.close()


def DBInsertMeal( user_id, mdate, mtime, name, calories ):
    conn = mysql.connector.connect(
        database = cfg.APP_DB_DATABASE,
        host = cfg.APP_DB_HOST,
        user = cfg.APP_DB_USER,
        passwd = cfg.APP_DB_PASS
    )
    dbcursor = conn.cursor()

    sql = "INSERT INTO `meals` (`user_id`, `mdate`, `mtime`, `name`, `calories`) VALUES (%s, %s, %s, %s, %s)"
    val = ( user_id, mdate, mtime, name, calories )

    dbcursor.execute( sql, val )
    conn.commit()

    dbcursor.close()
    conn.close()
