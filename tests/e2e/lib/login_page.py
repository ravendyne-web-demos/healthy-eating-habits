## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass
from locators import LoginPageLocators

from selenium.webdriver.support import expected_conditions as expected



class LoginPage(LibBaseClass):

    def visible(self):
        def cond( driver ):
            return \
                expected.visibility_of_element_located( LoginPageLocators.IDENTITY_FIELD )( driver ) and \
                expected.visibility_of_element_located( LoginPageLocators.PASSWORD_FIELD )( driver ) and \
                expected.visibility_of_element_located( LoginPageLocators.LOGIN_BUTTON )( driver )
        return cond

    def fill_form(self, identity, password):
        driver = self.driver

        elem = driver.find_element( *LoginPageLocators.IDENTITY_FIELD )
        elem.send_keys( identity )

        elem = driver.find_element( *LoginPageLocators.PASSWORD_FIELD )
        elem.send_keys( password )

    def do_login(self):
        driver = self.driver

        elem = driver.find_element( *LoginPageLocators.LOGIN_BUTTON )
        elem.click()

