## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import unittest

# import all test modules
import tests.app_login
import tests.member_meals_crud
import tests.member_meals_filter
import tests.manager_users_crud
import tests.admin_users_crud


# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests from modules to the test suite
suite.addTests( loader.loadTestsFromModule( tests.app_login ) )
suite.addTests( loader.loadTestsFromModule( tests.member_meals_crud ) )
suite.addTests( loader.loadTestsFromModule( tests.member_meals_filter ) )
suite.addTests( loader.loadTestsFromModule( tests.manager_users_crud ) )
suite.addTests( loader.loadTestsFromModule( tests.admin_users_crud ) )

# initialize a runner and run our test suite with it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )
