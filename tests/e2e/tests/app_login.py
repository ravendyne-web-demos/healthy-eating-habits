## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.login_page import LoginPage
from lib.toolbar import Toolbar
from lib.meals import MemberMealsTable
from lib.users import ManagerUsersTable, AdminUsersTable

from lib.locators import LoginPageLocators

class AppLogin(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait( 5 ) # seconds

    def test_login_without_credentials(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.do_login()
        # time.sleep( 3 ) #secs

        ## TODO assert error message visible

    def test_login_with_credentials(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )

    def test_login_as_member(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        meals_table = MemberMealsTable( driver )
        wait.until( meals_table.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def test_login_as_manager(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.MANAGER_IDENTITY, cfg.MANAGER_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        users_table = ManagerUsersTable( driver )
        wait.until( users_table.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def test_login_as_admin(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        users_table = AdminUsersTable( driver )
        wait.until( users_table.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def tearDown(self):
        self.driver.close()

