## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.app_helpers import LocalDateString, LocalTimeString
from lib.app_helpers import ISODateString, ISOTimeString
from lib.app_helpers import DBResetData, DBInsertMeal
from lib.app_helpers import AppHelper

from lib.meals import MemberMealsFilterForm, MemberMealsTable
from lib.locators import AppFormLocators


class MemberMealsFilter(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        DBResetData()


    def test_filter_reset(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 14, 15, 0 ), 'my meal A', '3000' )

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        meals_filter_form = MemberMealsFilterForm( driver )
        wait.until( meals_filter_form.visible() )

        meals_filter_form.fill_form( LocalDateString( 2019, 6, 14 ), LocalDateString( 2019, 6, 14 ), LocalTimeString( 14, 15, 0 ), LocalTimeString( 17, 15, 0 ) )
        # time.sleep( 3 ) #secs

        form_values = meals_filter_form.get_field_values()
        self.assertTrue( form_values[0] )
        self.assertTrue( form_values[1] )
        self.assertTrue( form_values[2] )
        self.assertTrue( form_values[3] )

        meals_filter_form.do_reset()
        # time.sleep( 3 ) #secs

        form_values = meals_filter_form.get_field_values()
        self.assertFalse( form_values[0] )
        self.assertFalse( form_values[1] )
        self.assertFalse( form_values[2] )
        self.assertFalse( form_values[3] )

        app_helper.logout()


    def test_display_all(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal C', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 15, 15, 0 ), 'my meal D', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        self.assertTrue( meals_table.contains_row_with_text( 'my meal B' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal D' ) )

        app_helper.logout()


    def test_filter_date_from(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal C', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 15, 15, 0 ), 'my meal D', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.fill_form( LocalDateString( 2019, 6, 15 ), None, None, None )
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal B' ) )

        self.assertTrue( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal D' ) )

        self.assertFalse( meals_table.contains_row_with_text( 'my meal A' ) )

        app_helper.logout()


    def test_filter_date_to(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal C', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 15, 15, 0 ), 'my meal D', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.fill_form( None, LocalDateString( 2019, 6, 15 ), None, None )
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_not_contain_row_with_text( 'my meal C' ) )

        self.assertTrue( meals_table.contains_row_with_text( 'my meal B' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal A' ) )

        self.assertFalse( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertFalse( meals_table.contains_row_with_text( 'my meal D' ) )

        app_helper.logout()


    def test_filter_time_from(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal C', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 15, 15, 0 ), 'my meal D', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.fill_form( None, None, LocalTimeString( 13, 0, 0 ), None )
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal B' ) )

        self.assertTrue( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal D' ) )

        self.assertFalse( meals_table.contains_row_with_text( 'my meal A' ) )

        app_helper.logout()


    def test_filter_time_to(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal C', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 15, 15, 0 ), 'my meal D', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.fill_form( None, None, None, LocalTimeString( 13, 0, 0 ) )
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        self.assertFalse( meals_table.contains_row_with_text( 'my meal B' ) )
        self.assertFalse( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertFalse( meals_table.contains_row_with_text( 'my meal D' ) )

        app_helper.logout()


    def test_filter_date_time(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 12, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 12, 15, 0 ), 'my meal B', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 15 ), ISOTimeString( 13, 15, 0 ), 'my meal C', '2000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 12, 15, 0 ), 'my meal D', '1000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 16 ), ISOTimeString( 14, 15, 0 ), 'my meal E', '500' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 17 ), ISOTimeString( 13, 15, 0 ), 'my meal F', '500' )

        app_helper = AppHelper( driver )
        meals_filter_form = MemberMealsFilterForm( driver )
        meals_table = MemberMealsTable( driver )

        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( meals_filter_form.visible() )

        meals_filter_form.do_reset()
        meals_filter_form.fill_form(
            LocalDateString( 2019, 6, 15 ),
            LocalDateString( 2019, 6, 16 ),
            LocalTimeString( 12, 0, 0 ),
            LocalTimeString( 14, 0, 0 )
        )
        meals_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal B' ) )

        self.assertTrue( meals_table.contains_row_with_text( 'my meal C' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal D' ) )

        self.assertFalse( meals_table.contains_row_with_text( 'my meal A' ) )
        self.assertFalse( meals_table.contains_row_with_text( 'my meal E' ) )
        self.assertFalse( meals_table.contains_row_with_text( 'my meal F' ) )

        app_helper.logout()


    def tearDown(self):
        self.driver.close()

