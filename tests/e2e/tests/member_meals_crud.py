## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.app_helpers import LocalDateString, LocalTimeString
from lib.app_helpers import ISODateString, ISOTimeString
from lib.app_helpers import DBResetData, DBInsertMeal
from lib.app_helpers import AppHelper

from lib.meals import MemberMealsForm, MemberMealsTable
from lib.locators import AppFormLocators


class MemberMealsCRUD(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        DBResetData()


    def test_row_click_fills_in_form(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 14, 15, 0 ), 'my meal A', '3000' )

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        meals_form = MemberMealsForm( driver )
        wait.until( meals_form.visible() )

        meals_table = MemberMealsTable( driver )
        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        meals_form.do_reset()
        # time.sleep( 3 ) #secs
        form_values = meals_form.get_field_values()
        self.assertFalse( form_values[0] )
        self.assertFalse( form_values[1] )
        self.assertFalse( form_values[2] )
        self.assertFalse( form_values[3] )

        elem = meals_table.find_row_with_text( 'my meal A' )

        meals_table.click_row( elem )

        form_values = meals_form.get_field_values()
        self.assertTrue( form_values[0] )
        self.assertTrue( form_values[1] )
        self.assertTrue( form_values[2] )
        self.assertTrue( form_values[3] )

        app_helper.logout()


    def test_add(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        meals_form = MemberMealsForm( driver )
        wait.until( meals_form.visible() )

        meals_form.fill_form( LocalDateString( 2019, 6, 14 ), LocalTimeString( 14, 15, 0 ), 'my meal A', '3000' )
        meals_form.do_add()

        meals_table = MemberMealsTable( driver )

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        app_helper.logout()


    def test_update(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 14, 15, 0 ), 'my meal A', '3000' )

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        meals_form = MemberMealsForm( driver )
        wait.until( meals_form.visible() )

        meals_table = MemberMealsTable( driver )
        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        elem = meals_table.find_row_with_text( 'my meal A' )

        meals_table.click_row( elem )

        meals_form.fill_form( None, None, 'my meal B', None )
        meals_form.do_update()

        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal B' ) )
        # time.sleep( 5 ) #secs
        self.assertTrue( meals_table.doesnt_contain_row_with_text( 'my meal A' ) )

        app_helper.logout()


    def test_delete(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 14, 15, 0 ), 'my meal A', '3000' )
        DBInsertMeal( cfg.MEMBER_DB_ID, ISODateString( 2019, 6, 14 ), ISOTimeString( 14, 15, 0 ), 'my meal B', '3000' )

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs

        meals_form = MemberMealsForm( driver )
        wait.until( meals_form.visible() )

        meals_table = MemberMealsTable( driver )
        wait.until( meals_table.expected_to_contain_row_with_text( 'my meal A' ) )

        elem = meals_table.find_row_with_text( 'my meal A' )
        # find 'x' button and click it
        elem = meals_table.find_row_delete_button( elem )
        elem.click()

        wait.until( meals_table.expected_to_not_contain_row_with_text( 'my meal A' ) )
        self.assertTrue( meals_table.contains_row_with_text( 'my meal B' ) )
        # time.sleep( 5 ) #secs

        app_helper.logout()


    def tearDown(self):
        self.driver.close()

