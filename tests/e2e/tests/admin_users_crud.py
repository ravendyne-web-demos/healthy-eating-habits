## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.app_helpers import DBResetUsers
from lib.app_helpers import AppHelper

from lib.toolbar import Toolbar
from lib.login_page import LoginPage
from lib.users import AdminUsersForm, AdminUsersTable
from lib.locators import AppFormLocators


class AdminUsersCRUD(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        DBResetUsers()


    def test_row_click_fills_in_form(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        users_form = AdminUsersForm( driver )
        users_table = AdminUsersTable( driver )

        app_helper.login( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( users_form.visible() )

        wait.until( users_table.expected_to_contain_row_with_text( 'member_user' ) )

        users_form.do_reset()
        # time.sleep( 3 ) #secs
        form_values = users_form.get_field_values()
        self.assertFalse( form_values[0] )
        self.assertFalse( form_values[1] )
        self.assertFalse( form_values[2] )
        # group field always has a value selected
        self.assertTrue( form_values[3] )

        elem = users_table.find_row_with_text( 'member_user' )

        users_table.click_row( elem )
        # time.sleep( 3 ) #secs

        form_values = users_form.get_field_values()
        self.assertTrue( form_values[0] )
        self.assertTrue( form_values[1] )
        # password field should become empty when we click on user row
        self.assertFalse( form_values[2] )
        self.assertTrue( form_values[3] )

        app_helper.logout()


    def test_add(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        users_form = AdminUsersForm( driver )
        users_table = AdminUsersTable( driver )

        app_helper.login( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( users_form.visible() )

        users_form.fill_form( 'new_username', 'new_username@email.com', 'new_password', cfg.MANAGER_DB_ID )
        users_form.do_add()

        wait.until( users_table.expected_to_contain_row_with_text( 'new_username' ) )

        app_helper.logout()

        login_page = LoginPage( driver )
        wait.until( login_page.visible() )

        app_helper.login( 'new_username@email.com', 'new_password' )
        wait.until( Toolbar( driver ).visible() )
        app_helper.logout()


    def test_update(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        users_form = AdminUsersForm( driver )
        users_table = AdminUsersTable( driver )

        app_helper.login( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( users_form.visible() )

        wait.until( users_table.expected_to_contain_row_with_text( 'other_member@heh.com' ) )

        elem = users_table.find_row_with_text( 'other_member@heh.com' )
        users_table.click_row( elem )

        users_form.fill_form( None, 'other_member@heh_heh.com', None, None )
        # time.sleep( 5 ) #secs
        users_form.do_update()

        wait.until( users_table.expected_to_contain_row_with_text( 'other_member@heh_heh.com' ) )
        # time.sleep( 5 ) #secs
        self.assertTrue( users_table.doesnt_contain_row_with_text( 'other_member@heh.com' ) )

        app_helper.logout()


    def test_update_group(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        users_form = AdminUsersForm( driver )
        users_table = AdminUsersTable( driver )

        app_helper.login( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( users_form.visible() )

        wait.until( users_table.expected_to_contain_row_with_text( 'other_member@heh.com' ) )

        elem = users_table.find_row_with_text( 'other_member@heh.com' )
        users_table.click_row( elem )

        users_form.fill_form( None, None, None, cfg.MANAGER_DB_ID )
        # time.sleep( 5 ) #secs
        users_form.do_update()

        users_form.do_reset()
        elem = users_table.find_row_with_text( 'other_member@heh.com' )
        users_table.click_row( elem )
        # time.sleep( 3 ) #secs

        form_values = users_form.get_field_values()
        # group field value
        self.assertEqual( form_values[3], '3' )

        app_helper.logout()


    def test_delete(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        app_helper = AppHelper( driver )
        users_form = AdminUsersForm( driver )
        users_table = AdminUsersTable( driver )

        app_helper.login( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( users_form.visible() )

        wait.until( users_table.expected_to_contain_row_with_text( 'another_member_user' ) )
        self.assertTrue( users_table.contains_row_with_text( 'member_user' ) )
        self.assertTrue( users_table.contains_row_with_text( 'manager' ) )

        elem = users_table.find_row_with_text( 'another_member_user' )
        # find 'x' button and click it
        elem = users_table.find_row_delete_button( elem )
        elem.click()

        wait.until( users_table.expected_to_not_contain_row_with_text( 'another_member_user' ) )
        # time.sleep( 5 ) #secs
        self.assertTrue( users_table.contains_row_with_text( 'member_user' ) )
        self.assertTrue( users_table.contains_row_with_text( 'manager' ) )

        app_helper.logout()


    def tearDown(self):
        self.driver.close()

