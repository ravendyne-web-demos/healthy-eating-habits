import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time
from datetime import date as ddate, time as dtime

from lib.app_helpers import LocalDateString, LocalTimeString, DBResetData
from lib.toolbar import Toolbar
from lib.app_helpers import AppHelper

from lib.meals import MemberMealsForm, MemberMealsTable


class Sandbox(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_sandbox(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBResetData()

        app_helper = AppHelper( driver )
        app_helper.login( cfg.MEMBER_IDENTITY, cfg.MEMBER_PASSWORD )
        # time.sleep( 3 ) #secs


        meals_form = MemberMealsForm( driver )
        wait.until( meals_form.visible() )

        meals_form.fill_form( LocalDateString( 2019, 6, 14 ), LocalTimeString( 14, 15, 0 ), 'my meal A', '3000' )
        time.sleep( 5 ) #secs
        meals_form.fill_form( None, None, 'my meal B', None )
        time.sleep( 5 ) #secs
        # meals_form.do_add()

        # meals_table = MemberMealsTable( driver )
        # wait.until( meals_table.contains_meal_with_name( 'my meal A' ) )

        time.sleep( 5 ) #secs

        app_helper.logout()
        # time.sleep( 3 ) #secs


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()

