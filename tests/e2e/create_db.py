## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib.app_helpers import RunSQLFile

import mysql.connector


def create_db():

    conn = mysql.connector.connect(
        # database='food_site',
        host="localhost",
        user="dev",
        passwd="devpass"
    )

    RunSQLFile( conn, '../db/create.sql' )
    RunSQLFile( conn, '../db/users.sql' )
    RunSQLFile( conn, '../db/meals.sql' )
    RunSQLFile( conn, '../db/totals.sql' )
    RunSQLFile( conn, '../db/settings.sql' )

    conn.close()

if __name__ == "__main__":
    create_db()
