#!/bin/bash

source ~/Development/pyenv/bin/activate

python create_db.py
python tests.py

# python -m unittest tests.app_login.AppLogin.test_login_without_credentials
# python -m unittest tests.member_meals_crud.MemberMealsCRUD.test_delete
# python -m unittest tests.member_meals_filter.MemberMealsFilter.test_display_all
# python -m unittest tests.manager_users_crud.ManagerUsersCRUD.test_row_click_fills_in_form
# python -m unittest tests.admin_users_crud.AdminUsersCRUD.test_row_click_fills_in_form
