## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import os

_CFG_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )

APP_DB_HOST = 'localhost'
APP_DB_USER = 'dev'
APP_DB_PASS = 'devpass'
APP_DB_DATABASE = 'food_site'

APP_DB_USERS_SQL_FILE = _CFG_LOCATION + '/../db/users.sql'

APP_BASE_URL = 'http://localhost:8080/site/'

MEMBER_IDENTITY = 'member@heh.com'
MEMBER_PASSWORD = 'password'
MEMBER_DB_ID = '2'

OTHER_MEMBER_IDENTITY = 'other_member@heh.com'
OTHER_MEMBER_PASSWORD = 'password'
OTHER_MEMBER_DB_ID = '4'

MANAGER_IDENTITY = 'manager@heh.com'
MANAGER_PASSWORD = 'password'
MANAGER_DB_ID = '3'

ADMIN_IDENTITY = 'admin@heh.com'
ADMIN_PASSWORD = 'password'
ADMIN_DB_ID = '1'

