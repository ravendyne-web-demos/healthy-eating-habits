# Docs for the app dev

## Structure and design essentials

Application is desigend on a client-server model where client side code lives in a web browser and server side code lives on a web server.

Client side is stateless and is only responsible for correctly rendering UI based on responses received from server side.

Server side is implementation of an API paradigm, i.e. everything the server ever serves are responses to API queries.

In accordance with this design, the application has this main structure:

- server side:
    - API implementation
    - implemented on web server
- client side:
    - UI rendering and display logic implementation
        - rendering / desing
        - display logic / UX
    - remoting layer responsible for communication with server-side API
    - implemented in web browser

## Technologies and frameworks used

On server side:

- HTTP POST-based API with JSON data payload in HTTP message body
- PHP and Mysql
- [CodeIgniter](https://www.codeigniter.com/user_guide/) PHP framework
- [Ion Auth](http://benedmunds.com/ion_auth/) authentication library for use with CodeIgniter

On client side:

- AJAX-based layer for communication with server API
- Javascript and HTML
- [Bootstrap 4](https://getbootstrap.com/docs/4.3/getting-started/introduction/) UI framework
- [jQuery](https://jquery.com/) Javascript library
- [Tempus Dominus Bootstrap 4 Datetime Picker](https://github.com/tempusdominus/bootstrap-4) component

## Source code structure

There are three folders in the project that contain all of the source code:

- `api`
- `jssrc`
- `site`

### `api` folder

This folder contains server-side code.

`api/application` folder contains application source and all the files related to project development are here.

`api/system` folder contains `CodeIgniter` source code and should be left as is.

Application source in `api/application` is spread accross following folders:

- `controllers`
- `core`
- `libraries`

`third_party` folders contains `Ion Auth` library files and `config` and `views` contain standard essential files used by a `CodeIgniter`-based application.

**`controllers`** folder contains API front-end code that directly processes incoming API requests and serves responses back to the client.
Controllers represent a translation layer between outside world and server-side application business logic.

**`core`** folder contains one file: `API_Controller.php` which contains base class for all API controller classes.

**`libraries`** folder contains all API business logic. This layer is coded as a separate module comprised out of a set of classes each responsible for specific part of operations:

- authentication: `Site_auth_service.php`
- user records management: `Site_meals_service.php`
- user management: `Site_users_service.php`
- user settings: `Site_settings_service.php`


### `site` folder

This folder contains the **UI** part of client-side code.

Entire UI is made of one file making this project effectively a one-page application.

This one file is broken down into pieces for ease of handling it during development. The pieces are more-or-less related to specific parts of UI:

- main file `index.php` which pulls in all the pieces by using PHP `include()` directive
- modal message box
- views (implemented as Bootstrap panels):
    - login
    - registration
    - admin UI
    - user UI
    - manager UI
    - `loading...` view
- top card (settings forma and log out button)
- bottom card (a.k.a. the footer)
- container head, should contain site-wide links and stuff (i.e. link back to main site, about link etc.) but is empty for now

Out of all of these, the only file that has `.php` extension is `indedx.php` because we need it to be processed by PHP on the server. The only reason we need it to be processed on the server is so we can *`include()`* all the other pieces.

All other files have `.html` extension in order to leverage HTML editing capabilities in whatever IDE/editor you may be using for development.

Support files for the main page are in these folders:

- `js` all Javascript files
- `css` all CSS files
- `img` all images
- `webfonts` all the fonts used


### `jssrc` folder

This folder contains the **UI logic** part of client-side code.

All UI logic is coded in vanilla Javascript with the use of jQuery to access DOM and a few standard Bootstrap 4 jQuery plugins required to operate Bootstrap HTML components/constructs (i.e. list tabs, modal box etc.).

Client side logic is coded by using the Javascript module pattern which is implemented via anonymous closure construct, one of the fundamental features of Javascript scripting language.

All of the code is processed and bundled together using [RollupJS](https://rollupjs.org/guide/en/).

In order to be able to bundle the source together, supporting libraries must be installed by running:

    $ npm install

in terminal open in `jssrc` folder.

Once all dependencies are installed, client side Javascript can be built by running:

    $ npm run build

This will:

- bundle up all code into `/site/js/heh-site.js` file and
- copy `site.js` file into `/site/js` folder

All client-side source resides in:

- `jssrc/src` folder
- `jssrc/site.js` files

The `jssrc/site.js` file contains a number of convenience functions created just to keep HTML templates a bit cleaner. All of these are global scope functions and are prefixed with `HEH_` to avoid potential collision. We could completely do without any of these functions and simply use the code contained in them directly. The reason they are here is more organizational than functional, it seems easier to write and read this:

    onclick="HEH_do_login();"

than this:

    onclick="HEH.PanelSwitcher().resetAllAround(); HEH.LoginForm().doLogin().then( function(){$('#top-card').addClass('d-none');} );"

`jssrc/src` folder has following sub-folders:

- `main` - contains all core client side code
- `util` - contains all general utilities used by code in `main`

The code in `main` folder is divided into few modules:

- `LoginForm` - handles login panel defined in `site/pane_login.html`
- `MealsForm` - handles a HTML form for editing/adding meals records, used in `site/pane_member.html` and `site/pane_admin.html`
- `RegistrationForm` - handles login panel defined in `site/pane_register.html`
- `SettingsForm` - handles a HTML form for editing settings, used in `site/top_card.html`
- `UserForm` - handles a HTML form for editing/adding users, used in `site/pane_manager.html` and `site/pane_admin.html`
- `PanelSwitcher` - logic to switch between different panels representing different parts of application UI, and for displaying system message box
- `Remote` - a layer that all server-side API calls go through

Client side is implemented as a stateless UI, what will be shown in the UI depends on user input and content of API call responses.

If, for whatever reason, a statefull client side implementation is required, it can be achieved by re-implementing `Remote.js` module to store and retrieve state in a local (web browser) storage and reconcile with server side code via API calls when needed.


## Tests

Folder `tests` contains unit and e2e tests for the application. Unit tests are in `tests/unit` and e2e in `tests/e2e`.

Unit tests test server side API business layer code with source in `api/application/libraries` folder. Tests are coded in PHP and instrumented by [PHPUnit](https://phpunit.de/) framework.

End-to-ens tests are Selenium tests instrumented by Python via [Selenium Python](https://selenium-python.readthedocs.io/) library.

### Unit tests

Unit tests test server-side API business layer logic. Controller code is not unit tested because it is coded as a pass-through layer that only wraps and unwraps whatever business layer code produces.

In order to run unit tests, PHP 7 has to be installed (Ubuntu/Debian installation example):

    $ sudo apt-get install php-cli

Above line will install php **without** also installing Apache web server and a lot of other *optional* dependencies. Once PHP is available, all test can be ran with:

    $ ./run.sh

or

    $ ./phpunit  --bootstrap bootstrap.php tests

Tests themselves are in `tests/unit/tests` folder and mock classes used to rig the test objects are in `tests/unit/mocks` folder.

### End-to-end tests

End-to-end tests are essentially application use case tests: each test exercises a specific use case.

In order to run e2e tests, Python has to be available with few dependencies installed (Ubuntu/Debian installation example):

    $ sudo apt-get install python python-pip
    $ sudo pip install mysql-connector-python selenium

Besides this, you have to have appropriate WebDriver binary somewhere in your path in order for Selenium to work. See [Selenium Python Instaation](https://selenium-python.readthedocs.io/installation.html#drivers) for more details what needs to be done there.

Once all is in place, e2e tests can be ran with:

    $ cd tests/e2e
    $ python create_db.py
    $ python tests.py

`create_db.py` needs to be ran only once to create the DB itself. DB connection parameters can be set in `e2e_config.py` file.

To create a DB super-user for development (in order not to use `root` account), you can use `tests/db/dev.sql` script. It will create `dev` user with password `devpass` that has all privileges granted on the entier database. 


## Database

Database used for this app is Mysql. Once installed, it needs to be set up for use by application. Easiest way to do this is to use SQL scripts from `tests/db` folder.

You will have to execute following scripts, in this order:

- `create.sql`
- `users.sql`
- `meals,sql`
- `settings.sql`

If you have Python and Mysql-Connector installed (as specified in E2E tests section) you can get this done by running:

    $ cd tests/e2e
    $ python create_db.py

This will create and populate all tables needed for the application to run.

If you only want to have **admin** user available initially, you can create the DB as described and then delete all user records from `users` table but the one with `id` == 1, and all entries from `users_groups` table except the one with `id` == 1. After that dump the database and use the dump to initialize an empty DB later when needed.

Scripts in `tests/e2e/db` folder are meant for e2e tests and will create a minimal number of users and data needed to execute the tests.


## Initial application data

If you use `tests/e2e/db` scripts (i.e. by running `python create_db.py`) to initialize DB, there will be 4 users available to log in:

- The **administrator** - id: `admin@heh.com`, pw: `password`
- A **manager** - id: `manager@heh.com`, pw: `password`
- A **user** - id: `member@heh.com`, pw: `password`
- Another user - id: `other_member@heh.com`, pw: `password`

You can use these credentials to log in and work with different aspects of the application.

