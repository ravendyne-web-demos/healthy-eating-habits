# Docs for the app user

## Login and registration

In order to start working with the application, you need to log in first by entering your email and password:

![login](img/Selection_001.png)

By clicking on `Login` button, you will log in.

If you don't have an account yet, you can register by clicking on the registration link:

![registration link](img/Selection_002.png)

Fill in your username, email and password and click `Register`:

![registration](img/Selection_003.png)

If there are any issues, a message box will open:

![registration message](img/Selection_007.png)

If registration is successfull, login page will be displayed where you need to enter your email and password in order to log in.

## Members

Once you log in, if you have a member account, you will be presented with a list of your meal entries.

![member](img/Selection_004.png)

You can set up your daily calory target in the settings box at the top

![member](img/Selection_009.png)

All meals that belong to a day when you were **under** your daily target will be colored in **green**.

All meals that belong to a day when you were **over** your daily target will be colored in **red**.

Clicking on an entry will populate meal edit form where you can change meal parameters and save it by clicking `Update`.

![member](img/Selection_008.png)

You can add new meal by filling in the values and clicking `Add`.

The form fields can be cleared by clicking on `Reset`.

You can view only meals from specific days and/or time of day by using the filter form.

![member](img/Selection_010.png)

If you leave all fields empty and click on `Filter entries`, **all** your meals will be displayed.

If you want to limit your display to certain date range, fill in `Date From` and `Date To` fields. This will display all meals between those two dates *including* the meals on those dates. If you want to see meals for one specific day, enter the same date in both `Date From` and `Date To` fields.

If you want to limit your display to certain time range, fill in `Time From` and `Time To` fields. This will display all meals between those two times of a day.

Date and time fields are independant, i.e filling in `2019-06-14` in `Date From` and `10:00` in `Time From` will **not** give you all meals **since** `10:00` in the morning on `2019-06-14` date.

Instead, it will give you all meals eaten **after** `10:00`, for **all** days, **starting** with day on date `2019-06-14`.

This behaviour makes it easy to i.e. display how many calories you had before noon every day in the last month.


## Manager

Once you log in, if you have a manager account, you will be presented with a list of application users.

![manager](img/Selection_005.png)

You can edit a user by clicking on that row, changing edit form field contents and clicking `Update` button.

You can add new user using `Add` button.

Managers can only view, edit and add `Member` users.

By clicking on a user record, meals table will display that user's meals. You can then edit, update and add meal records for the selected user.


## Administrator

If you logged in as the administrator, you will be presented with a list of users and their meal records.

![admin](img/Selection_006.png)

Administrators can view, edit and add **both** user types: `Member` and `Manager`, and edit **all** meal records.

There is **only one** administrator account and that one can only be changed by directly manipulating the user record with `ID` == 1 in the database.

