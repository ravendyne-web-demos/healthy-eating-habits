(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.HEH = global.HEH || {})));
}(this, (function (exports) { 'use strict';

    // Polyfills

    if ( Number.EPSILON === undefined ) {

    	Number.EPSILON = Math.pow( 2, - 52 );

    }

    if ( Number.isInteger === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger

    	Number.isInteger = function ( value ) {

    		return typeof value === 'number' && isFinite( value ) && Math.floor( value ) === value;

    	};

    }

    //

    if ( Math.sign === undefined ) {

    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sign

    	Math.sign = function ( x ) {

    		return ( x < 0 ) ? - 1 : ( x > 0 ) ? 1 : + x;

    	};

    }

    if ( Function.prototype.name === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/name

    	Object.defineProperty( Function.prototype, 'name', {

    		get: function () {

    			return this.toString().match( /^\s*function\s*([^\(\s]*)/ )[ 1 ];

    		}

    	} );

    }

    if ( Object.assign === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign

    	( function () {

    		Object.assign = function ( target ) {

    			'use strict';

    			if ( target === undefined || target === null ) {

    				throw new TypeError( 'Cannot convert undefined or null to object' );

    			}

    			var output = Object( target );

    			for ( var index = 1; index < arguments.length; index ++ ) {

    				var source = arguments[ index ];

    				if ( source !== undefined && source !== null ) {

    					for ( var nextKey in source ) {

    						if ( Object.prototype.hasOwnProperty.call( source, nextKey ) ) {

    							output[ nextKey ] = source[ nextKey ];

    						}

    					}

    				}

    			}

    			return output;

    		};

    	} )();

    }

    if( Array.prototype.includes === undefined ) {

    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes#Polyfill

    	// https://tc39.github.io/ecma262/#sec-array.prototype.includes
    	Object.defineProperty( Array.prototype, 'includes', {
    		value: function( valueToFind, fromIndex ) {
    	
    			if (this == null) {
    				throw new TypeError('"this" is null or not defined');
    			}
    	
    			// 1. Let O be ? ToObject(this value).
    			var o = Object(this);
    	
    			// 2. Let len be ? ToLength(? Get(O, "length")).
    			var len = o.length >>> 0;
    	
    			// 3. If len is 0, return false.
    			if (len === 0) {
    				return false;
    			}
    	
    			// 4. Let n be ? ToInteger(fromIndex).
    			//    (If fromIndex is undefined, this step produces the value 0.)
    			var n = fromIndex | 0;
    	
    			// 5. If n ≥ 0, then
    			//  a. Let k be n.
    			// 6. Else n < 0,
    			//  a. Let k be len + n.
    			//  b. If k < 0, let k be 0.
    			var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    	
    			function sameValueZero(x, y) {
    				return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
    			}
    	
    			// 7. Repeat, while k < len
    			while (k < len) {
    				// a. Let elementK be the result of ? Get(O, ! ToString(k)).
    				// b. If SameValueZero(valueToFind, elementK) is true, return true.
    				if (sameValueZero(o[k], valueToFind)) {
    					return true;
    				}
    				// c. Increase k by 1. 
    				k++;
    			}
    	
    			// 8. Return false
    			return false;
    		}
    	});
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach#Polyfill
    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if( Array.prototype.forEach === undefined ) {

    	Array.prototype.forEach = function(callback/*, thisArg*/) {

    		var T, k;

    		if (this == null) {
    			throw new TypeError('this is null or not defined');
    		}

    		// 1. Let O be the result of calling toObject() passing the
    		// |this| value as the argument.
    		var O = Object(this);

    		// 2. Let lenValue be the result of calling the Get() internal
    		// method of O with the argument "length".
    		// 3. Let len be toUint32(lenValue).
    		var len = O.length >>> 0;

    		// 4. If isCallable(callback) is false, throw a TypeError exception. 
    		// See: http://es5.github.com/#x9.11
    		if (typeof callback !== 'function') {
    			throw new TypeError(callback + ' is not a function');
    		}

    		// 5. If thisArg was supplied, let T be thisArg; else let
    		// T be undefined.
    		if (arguments.length > 1) {
    			T = arguments[1];
    		}

    		// 6. Let k be 0.
    		k = 0;

    		// 7. Repeat while k < len.
    		while (k < len) {

    			var kValue;

    			// a. Let Pk be ToString(k).
    			//    This is implicit for LHS operands of the in operator.
    			// b. Let kPresent be the result of calling the HasProperty
    			//    internal method of O with argument Pk.
    			//    This step can be combined with c.
    			// c. If kPresent is true, then
    			if (k in O) {

    				// i. Let kValue be the result of calling the Get internal
    				// method of O with argument Pk.
    				kValue = O[k];

    				// ii. Call the Call internal method of callback with T as
    				// the this value and argument list containing kValue, k, and O.
    				callback.call(T, kValue, k, O);
    			}
    			// d. Increase k by 1.
    			k++;
    		}
    		// 8. return undefined.
    	};
    }

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    var CONFIG = {

        TABS_LIST_ID: 'list-tabs',

        //
        // panels
        //
        PANEL_LOGIN_ID: 'panel-login',
        PANEL_MEMBERS_ID: 'panel-members',
        PANEL_MANAGER_ID: 'panel-manager',
        PANEL_ADMIN_ID: 'panel-admin',
        PANEL_LOADING_ID: 'panel-loading',
        PANEL_REGISTER_ID: 'panel-register',

        //
        // modal
        //
        SYSTEM_MODAL_ID: 'system-modal',

        //
        // forms
        //
        LOGIN_FORM_ID: 'login-form',
        REGISTRATION_FORM_ID: 'registration-form',
        MEMBER_MEAL_FORM_ID: 'member-meal-form',
        MEMBER_FILTER_FORM_ID: 'member-filter-form',
        USER_SETTINGS_FORM_ID: 'user-settings-form',
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

     /**
      * Removes all rows from `<table>` with given ID.
      * 
      * If the table has `<tr>` with a class set to `template-row`, that row will not be removed.
      * 
      * @param {string} table_id 
      */
     const DOMTableClear = function( table_id ) {

        var template_row = $(`#${table_id} tr.template-row`).detach();
        $(`#${table_id} tbody`).empty().append( template_row );                
    };

    /**
     * Returns a **clone** of the `tr` in this table that is marked as template row.
     * Class `d-none` (Bootstrap `display: none;` helper) is removed from it.
     * 
     * Template rows are marked as such by assigning them `template-row` CSS class.
     * 
     * @param {string} table_id 
     */
    const DOMTableTemplateRowClone = function( table_id ) {

        let template_row_clone = $(`#${table_id} tr.template-row`).first().clone();
        template_row_clone.removeClass('d-none');
        template_row_clone.removeClass('template-row');

        return template_row_clone;
    };

    const DOMTablePopulate = function( table_id, data_array, row_callback, column_callback ) {

        DOMTableClear( table_id );

        // clones template row element
        var template_row = DOMTableTemplateRowClone( table_id );
        var table_body = $(`#${table_id} tbody`);

        let row_number = 1;
        data_array.forEach(element => {

            let new_row = template_row.clone();

            row_callback( row_number, element, new_row );

            new_row.find('th').html( row_number );
            new_row.find('td').each( function( idx, el ){
                column_callback( idx, element, $(el) );
            });
            table_body.append( new_row );

            row_number++;
        });

        // discard cloned row element
        template_row.remove();
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    const DateLocalToISO = function( date_string ) {
        return moment( date_string, 'L' ).format( 'YYYY-MM-DD' );
    };
    const TimeLocalToISO = function( time_string ) {
        return moment( time_string, 'LT' ).format( 'HH:mm:ss' );
    };
    const DateISOToLocal = function( date_string ) {
        return moment( date_string, 'YYYY-MM-DD' ).format( 'L' );
    };
    const TimeISOToLocal = function( time_string ) {
        return moment( time_string, 'HH:mm:ss' ).format( 'LT' );
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function MealsForm( meal_form_id, filter_form_id, user_id )
    {
        //
        // Case when MealsForm is called as a function
        //
        if( ! ( this instanceof MealsForm ) )
        {
            return new MealsForm( meal_form_id, filter_form_id, user_id )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        if( meal_form_id === undefined ) {
            throw 'Required parameter "meal_form_id" is missing.';
        }
        if( filter_form_id === undefined ) {
            throw 'Required parameter "filter_form_id" is missing.';
        }
        let _meal_form_id = meal_form_id;
        let _filter_form_id = filter_form_id;
        let _user_id = user_id;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
        {
            meal_form_id:
            {
                get: function() { return _meal_form_id },
            },
            filter_form_id:
            {
                get: function() { return _filter_form_id },
            },
            user_id:
            {
                get: function() { return _user_id },
            },
        });
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( MealsForm.prototype,
    {
        populateTable: function( table_id, data_array ) {

            const self = this;

            DOMTablePopulate( table_id, data_array,
            // called for every <tr> element in the table
            function( tr_idx, data_el, dom_el ){
                // atatch meal ID to <tr>
                dom_el.data('mealId', data_el.id );
                dom_el.off( 'click' );
                dom_el.on( 'click', function(){
                    MealsForm( self.meal_form_id, self.filter_form_id, self.user_id ).setMealFormFields( data_el );
                });
                if( data_el.exceeded ) {
                    dom_el.addClass('table-danger');
                } else {
                    dom_el.addClass('table-success');
                }
            },
            // called for every <td> element in a row
            function( td_idx, data_el, dom_el ){
                switch( td_idx ) {
                    case 0:
                        // delete button
                        dom_el.find('button').off( 'click' );
                        dom_el.find('button').on( 'click', function(){
                            MealsForm( self.meal_form_id, self.filter_form_id, self.user_id ).deleteMeal( data_el.id, table_id );
                        });
                    break;
                    case 1:
                        dom_el.html( DateISOToLocal( data_el.mdate ) );
                    break;
                    case 2:
                        dom_el.html( TimeISOToLocal( data_el.mtime ) );
                    break;
                    case 3:
                        dom_el.html( data_el.name );
                    break;
                    case 4:
                        dom_el.html( data_el.calories );
                    break;
                }
            });
        },

        setMealFormFields: function( data ) {

            $(`#${this.meal_form_id} #meal-id`).val( data.id );
            $(`#${this.meal_form_id} #meal-date`).val( DateISOToLocal( data.mdate ) );
            $(`#${this.meal_form_id} #meal-time`).val( TimeISOToLocal( data.mtime ) );
            $(`#${this.meal_form_id} #meal-name`).val( data.name );
            $(`#${this.meal_form_id} #meal-calories`).val( data.calories );
        },

        getMealFormFields: function() {

            let meal_id = $(`#${this.meal_form_id} #meal-id`).val();
            let meal_date = $(`#${this.meal_form_id} #meal-date`).val();
            let meal_time = $(`#${this.meal_form_id} #meal-time`).val();
            let meal_name = $(`#${this.meal_form_id} #meal-name`).val();
            let meal_calories = $(`#${this.meal_form_id} #meal-calories`).val();

            meal_date = DateLocalToISO( meal_date );
            meal_time = TimeLocalToISO( meal_time );

            return {
                'id' : meal_id,
                'mdate' : meal_date,
                'mtime' : meal_time,
                'name' : meal_name,
                'calories' : meal_calories
            };
        },

        getMealFilterFields: function() {

            let filter = {};

            // populate filter options, if any
            let meal_date_from = $(`#${this.filter_form_id} #filter-date-from`).val();
            let meal_date_to = $(`#${this.filter_form_id} #filter-date-to`).val();
            let meal_time_from = $(`#${this.filter_form_id} #filter-time-from`).val();
            let meal_time_to = $(`#${this.filter_form_id} #filter-time-to`).val();

            if( meal_date_from ) {
                meal_date_from = DateLocalToISO( meal_date_from );
                filter['date_from'] = meal_date_from;
            }
            if( meal_date_to ) {
                meal_date_to = DateLocalToISO( meal_date_to );
                filter['date_to'] = meal_date_to;
            }
            if( meal_time_from ) {
                meal_time_from = TimeLocalToISO( meal_time_from );
                filter['time_from'] = meal_time_from;
            }
            if( meal_time_to ) {
                meal_time_to = TimeLocalToISO( meal_time_to );
                filter['time_to'] = meal_time_to;
            }

            return filter;
        },

        listMeals: function( table_id ) {

            const self = this;

            let filter = this.getMealFilterFields();

            let parameters = { "filter" : filter };
            if( self.user_id !== undefined ) {
                parameters.user_id = self.user_id;
            }

            const dfd = $.Deferred();

            Remote().meal_retrieve( parameters )
            .done(function( result )
            {
                if( result.success ) {

                    self.populateTable( table_id, result.meals );

                    dfd.resolve( result );

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        deleteMeal: function( meal_id, table_id ) {

            const self = this;

            const dfd = $.Deferred();

            Remote().meal_delete({ 'id': meal_id })
            .done(function( result ){

                // refresh meals table
                self.listMeals( table_id ).done(function(){
                    dfd.resolve( result );
                });

            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        addMeal: function( table_id ) {

            let meal_data = this.getMealFormFields();

            const self = this;

            if( self.user_id !== undefined ) {
                meal_data.user_id = self.user_id;
            }

            const dfd = $.Deferred();

            Remote().meal_create( meal_data )
            .done(function( result )
            {
                // console.log(result);
                if( result.success ) {

                    // refresh meals table
                    self.listMeals( table_id ).done(function(){
                        dfd.resolve( result );
                    });

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        updateMeal: function( table_id ) {

            let meal_data = this.getMealFormFields();

            const self = this;

            const dfd = $.Deferred();

            Remote().meal_update( meal_data )
            .done(function( result )
            {
                if( result.success ) {

                    // refresh meals table
                    self.listMeals( table_id ).done(function(){
                        dfd.resolve( result );
                    });

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function UserForm( user_form_id, for_admin )
    {
        //
        // Case when UserForm is called as a function
        //
        if( ! ( this instanceof UserForm ) )
        {
            return new UserForm( user_form_id, for_admin )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        if( user_form_id === undefined ) {
            throw 'Required parameter "user_form_id" is missing.';
        }
        let _user_form_id = user_form_id;
        // Admin user form view needs some more features so we flag that case here.
        // Not the best way of handling use cases for a class
        // but when there's a deadline looming, anything goes
        // as long as it's clear cut :)
        // A better way would be to i.e. have AdminUserForm() that extends UserForm()
        // and implements stuff specific to admin UI handling
        let _for_admin = for_admin;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
        {
            user_form_id:
            {
                get: function() { return _user_form_id },
            },
            for_admin:
            {
                get: function() { return _for_admin },
            },
        });
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( UserForm.prototype,
    {
        populateTable: function( table_id, data_array ) {

            const self = this;

            DOMTablePopulate( table_id, data_array,
            // called for every <tr> element in the table
            function( tr_idx, data_el, dom_el ){
                // atatch user ID to <tr>
                dom_el.data('userId', data_el.id );
                dom_el.off( 'click' );
                dom_el.on( 'click', function(){
                    UserForm( self.user_form_id ).setUserFormFields( data_el );
                    if( self.for_admin ) {
                        // last-minute, ugly hack for admin use case
                        MealsForm( 'admin-meal-form', 'admin-filter-form', data_el.id ).listMeals( 'admin-meals-table' );
                        PanelSwitcher().enableAdminButtons();
                    }
                });
            },
            // called for every <td> element in a row
            function( td_idx, data_el, dom_el ){
                switch( td_idx ) {
                    case 0:
                        // delete button
                        dom_el.find('button[name="user-delete-button"]').off( 'click' );
                        dom_el.find('button[name="user-delete-button"]').on( 'click', function(){
                            UserForm( self.user_form_id ).deleteUser( data_el.id, table_id );
                        });
                    break;
                    case 1:
                        dom_el.html( data_el.username );
                    break;
                    case 2:
                        dom_el.html( data_el.email );
                    break;
                }
            });
        },

        setUserFormFields: function( data ) {

            $(`#${this.user_form_id} #user-id`).val( data.id );
            $(`#${this.user_form_id} #user-username`).val( data.username );
            $(`#${this.user_form_id} #user-email`).val( data.email );
            if( data.manager ) {
                $(`#${this.user_form_id} #user-group`).val( '3' );
            } else {
                $(`#${this.user_form_id} #user-group`).val( '2' );
            }

            // clear password field
            $(`#${this.user_form_id} #user-password`).val();
        },

        getUserFormFields: function() {

            let user_id = $(`#${this.user_form_id} #user-id`).val();
            let user_username = $(`#${this.user_form_id} #user-username`).val();
            let user_email = $(`#${this.user_form_id} #user-email`).val();
            let user_password = $(`#${this.user_form_id} #user-password`).val();
            let user_group = $(`#${this.user_form_id} #user-group`).val();
            let is_manager = user_group == 3;

            let user_data = {};

            user_data['id'] = user_id || undefined;
            user_data['username'] = user_username || undefined;
            user_data['email'] = user_email || undefined;
            user_data['password'] = user_password || undefined;
            user_data['manager'] = is_manager;

            return user_data;
        },

        listUsers: function( table_id ) {

            const self = this;

            const dfd = $.Deferred();

            Remote().user_list( {} )
            .done(function( result )
            {
                if( result.success ) {

                    self.populateTable( table_id, result.users );

                    dfd.resolve( result );

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        deleteUser: function( user_id, table_id ) {

            const self = this;

            const dfd = $.Deferred();

            Remote().user_delete({ 'id': user_id })
            .done(function( result ){

                // refresh users table
                self.listUsers( table_id ).done(function(){
                    dfd.resolve( result );
                });

            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        addUser: function( table_id ) {

            let user_data = this.getUserFormFields();

            const self = this;

            const dfd = $.Deferred();

            Remote().user_create( user_data )
            .done(function( result )
            {
                // console.log(result);
                if( result.success ) {

                    // refresh users table
                    self.listUsers( table_id ).done(function(){
                        dfd.resolve( result );
                    });

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        updateUser: function( table_id ) {

            let user_data = this.getUserFormFields();

            const self = this;

            const dfd = $.Deferred();

            Remote().user_update( user_data )
            .done(function( result )
            {
                if( result.success ) {

                    // refresh users table
                    self.listUsers( table_id ).done(function(){
                        dfd.resolve( result );
                    });

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function SettingsForm( form_id )
    {
        //
        // Case when SettingsForm is called as a function
        //
        if( ! ( this instanceof SettingsForm ) )
        {
            return new SettingsForm( form_id )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        let _form_id = form_id || CONFIG.USER_SETTINGS_FORM_ID;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
        {
            form_id:
            {
                get: function() { return _form_id },
            },
        });
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( SettingsForm.prototype,
    {

        showForm: function() {

            $(`#${this.form_id}`).removeClass( 'd-none' );
        },

        hideForm: function() {

            $(`#${this.form_id}`).addClass( 'd-none' );
        },

        setFormFields: function( data ) {

            $(`#${this.form_id} #settings-calories`).val( data.calories || '' );
        },

        getFormFields: function() {

            let settings_calories = $(`#${this.form_id} #settings-calories`).val();

            let settings_data = {};

            settings_data['calories'] = settings_calories || undefined;

            return settings_data;
        },

        updateSettings: function() {

            let settings_data = this.getFormFields();

            const dfd = $.Deferred();

            Remote().settings_update( settings_data )
            .done(function( result )
            {
                if( result.success ) {

                    dfd.resolve( result );

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        retrieveSettings: function() {

            const self = this;

            const dfd = $.Deferred();

            Remote().settings_retrieve( {} )
            .done(function( result )
            {
                if( result.success ) {

                    let settings_data = result.data;
                    self.setFormFields( settings_data );

                    dfd.resolve( result );

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function PanelSwitcher( tabs_list_id )
    {
        //
        // Case when PanelSwitcher is called as a function
        //
        if( ! ( this instanceof PanelSwitcher ) )
        {
            return new PanelSwitcher( tabs_list_id )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        let _tabs_list_id = tabs_list_id || CONFIG.TABS_LIST_ID;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
            {
                tabs_list_id:
                {
                    get: function() { return _tabs_list_id },
                },
            });
        }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( PanelSwitcher.prototype,
    {
        showRegistration: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_REGISTER_ID}"]`).tab('show');
        },

        showLogin: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_LOGIN_ID}"]`).tab('show');
        },

        showMembers: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_MEMBERS_ID}"]`).tab('show');
        },

        showManager: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_MANAGER_ID}"]`).tab('show');
        },

        showAdmin: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_ADMIN_ID}"]`).tab('show');
        },

        showLoading: function() {
            $(`#${this.tabs_list_id} a[href="#${CONFIG.PANEL_LOADING_ID}"]`).tab('show');
        },

        disableAdminButtons: function() {
            $('#admin-meal-form button[name="meal-add-button"]').prop( 'disabled', true );
            $('#admin-meal-form button[name="meal-update-button"]').prop( 'disabled', true );
        },

        enableAdminButtons: function() {
            $('#admin-meal-form button[name="meal-add-button"]').removeProp( 'disabled' );
            $('#admin-meal-form button[name="meal-add-button"]').removeAttr( 'disabled' );
            $('#admin-meal-form button[name="meal-update-button"]').removeProp( 'disabled' );
            $('#admin-meal-form button[name="meal-update-button"]').removeAttr( 'disabled' );
        },

        resetAllAround: function() {

            DOMTableClear( 'member-meals-table' );
            DOMTableClear( 'admin-meals-table' );
            DOMTableClear( 'admin-users-table' );
            DOMTableClear( 'manager-users-table' );
        
            $('#member-meal-form').each(function(){
                this.reset();
            });
            $('#member-meal-form #meal-id').removeAttr('value');
        
            $('#admin-meal-form').each(function(){
                this.reset();
            });
            $('#admin-meal-form #meal-id').removeAttr('value');
        
            $('#member-filter-form').each(function(){
                this.reset();
            });
            $('#admin-filter-form').each(function(){
                this.reset();
            });
        
            $('#manager-user-form').each(function(){
                this.reset();
            });
            $('#manager-user-form #user-id').removeAttr('value');
        
            $('#admin-user-form').each(function(){
                this.reset();
            });
            $('#admin-user-form #user-id').removeAttr('value');

            this.disableAdminButtons();
        },

        showStartPanelForUser: function( user_data ) {

            if( user_data.groups.includes( 'admin' ) ) {

                SettingsForm( 'user-settings-form' ).hideForm();
                this.showAdmin();
                // true -> admin hack, see UserForm.for_admin for description
                UserForm( 'admin-user-form', true ).listUsers( 'admin-users-table' );

            } else if( user_data.groups.includes( 'managers' ) ) {

                SettingsForm( 'user-settings-form' ).hideForm();
                this.showManager();
                UserForm( 'manager-user-form' ).listUsers( 'manager-users-table' );

            } else if( user_data.groups.includes( 'members' ) ) {

                let settings_form = SettingsForm( 'user-settings-form' );
                settings_form.showForm();
                settings_form.retrieveSettings();
                this.showMembers();
                MealsForm( 'member-meal-form', 'member-filter-form' ).listMeals( 'member-meals-table' );

            } else {
                SettingsForm( 'user-settings-form' ).hideForm();
                // TODO show message 'unsupported group??'
                this.showMessage( `User "${user_data.username}" has no assigned application page` );
            }
        },

        showMessage: function( message ) {
            $(`#${CONFIG.SYSTEM_MODAL_ID} #message`).html(message);
            $(`#${CONFIG.SYSTEM_MODAL_ID}`).modal('show');
            console.error( message );
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    //
    // read-only properties in javascript
    // ¯\_(ツ)_/¯
    //

    // defined here, these will be initialized only once, on load
    let _AUTH = {};
    Object.defineProperties(_AUTH,
    {
        LOGIN:
        {
            get: function() { return '/api/index.php?/auth/login' },
        },
        LOGOUT:
        {
            get: function() { return '/api/index.php?/auth/logout' },
        },
        REGISTER:
        {
            get: function() { return '/api/index.php?/auth/register' },
        },
        WHO_AM_I:
        {
            get: function() { return '/api/index.php?/auth/who_am_i' },
        },
    });

    let _MEALS = {};
    Object.defineProperties(_MEALS,
    {
        CREATE:
        {
            get: function() { return '/api/index.php?/meals/create' },
        },
        RETRIEVE:
        {
            get: function() { return '/api/index.php?/meals/retrieve' },
        },
        UPDATE:
        {
            get: function() { return '/api/index.php?/meals/update' },
        },
        DELETE:
        {
            get: function() { return '/api/index.php?/meals/delete' },
        },
    });

    let _USER = {};
    Object.defineProperties(_USER,
    {
        CREATE:
        {
            get: function() { return '/api/index.php?/user/create' },
        },
        RETRIEVE:
        {
            get: function() { return '/api/index.php?/user/retrieve' },
        },
        UPDATE:
        {
            get: function() { return '/api/index.php?/user/update' },
        },
        DELETE:
        {
            get: function() { return '/api/index.php?/user/delete' },
        },
        LIST:
        {
            get: function() { return '/api/index.php?/user/list' },
        },
    });

    let _SETTINGS = {};
    Object.defineProperties(_SETTINGS,
    {
        RETRIEVE:
        {
            get: function() { return '/api/index.php?/settings/retrieve' },
        },
        UPDATE:
        {
            get: function() { return '/api/index.php?/settings/update' },
        },
    });

    let _API_URL = {};
    Object.defineProperties(_API_URL,
    {
        AUTH:
        {
            get: function() { return _AUTH },
        },
        MEALS:
        {
            get: function() { return _MEALS },
        },
        USER:
        {
            get: function() { return _USER },
        },
        SETTINGS:
        {
            get: function() { return _SETTINGS },
        },
    });

    //
    // REMOTE
    //
    function Remote(param) {
        //
        // Case when Remote is called as a function
        //
        if (!(this instanceof Remote)) {
            return new Remote(param)
        }

        Object.defineProperties(this,
        {
            API_URL:
            {
                get: function() { return _API_URL },
            },
        });
    }


    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Remote.prototype,
    {
        //
        // AUTH
        //
        login: function( identity, password, remember_me = false ) {
            return this.api_request( this.API_URL.AUTH.LOGIN, {
                "identity": identity,
                "password" : password,
                "remember_me" : remember_me
            });
        },

        logout: function() {
            return this.api_request( this.API_URL.AUTH.LOGOUT, {} );
        },

        register: function( data ) {
            return this.api_request( this.API_URL.AUTH.REGISTER, data );
        },

        get_logged_in_user: function() {
            return this.api_request_handler( this.API_URL.AUTH.WHO_AM_I, {} );
        },

        //
        // MEALS
        //
        meal_create: function( data ) {
            return this.api_request_handler( this.API_URL.MEALS.CREATE, data );
        },

        meal_retrieve: function( data ) {
            return this.api_request_handler( this.API_URL.MEALS.RETRIEVE, data );
        },

        meal_update: function( data ) {
            return this.api_request_handler( this.API_URL.MEALS.UPDATE, data );
        },

        meal_delete: function( data ) {
            return this.api_request_handler( this.API_URL.MEALS.DELETE, data );
        },

        //
        // USERS
        //
        user_create: function( data ) {
            return this.api_request_handler( this.API_URL.USER.CREATE, data );
        },

        user_retrieve: function( data ) {
            return this.api_request_handler( this.API_URL.USER.RETRIEVE, data );
        },

        user_list: function( data ) {
            return this.api_request_handler( this.API_URL.USER.LIST, data );
        },

        user_update: function( data ) {
            return this.api_request_handler( this.API_URL.USER.UPDATE, data );
        },

        user_delete: function( data ) {
            return this.api_request_handler( this.API_URL.USER.DELETE, data );
        },

        //
        // SETTINGS
        //
        settings_retrieve: function( data ) {
            return this.api_request_handler( this.API_URL.SETTINGS.RETRIEVE, data );
        },

        settings_update: function( data ) {
            return this.api_request_handler( this.API_URL.SETTINGS.UPDATE, data );
        },

        //
        // OTHER STUFF
        //
        api_request_handler: function( url, data ) {

            const self = this;
            const dfd = $.Deferred();

            this.api_request( url, data )
            .done( function( result ) {

                if( ! result.success && result.needs_login ) {
        
                    PanelSwitcher().showLogin();
        
                } else {
        
                    dfd.resolve( result );
                }
            })
            .fail( function( xhr ) {

                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject( xhr );
            });

            return dfd.promise();
        },

        api_request: function( url, data ) {
            return $.ajax({
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json',
                processData: false,
                type: 'POST',
                url: url
            });
        },

        toString: function() {
        },
    });

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function LoginForm( form_id )
    {
        //
        // Case when LoginForm is called as a function
        //
        if( ! ( this instanceof LoginForm ) )
        {
            return new LoginForm( form_id )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        let _form_id = form_id || CONFIG.LOGIN_FORM_ID;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
        {
            form_id:
            {
                get: function() { return _form_id },
            },
        });
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( LoginForm.prototype,
    {
        doLogin: function() {

            const dfd = $.Deferred();

            let identity = $(`#${this.form_id} #login-identity`).val();
            let password = $(`#${this.form_id} #login-password`).val();
            let remember_me = $(`#${this.form_id} #login-remember`).prop('checked');

            PanelSwitcher().showLoading();

            Remote().login( identity, password, remember_me )
            .done(function( result )
            {
                if( result.success ) {

                    PanelSwitcher().showStartPanelForUser( result.user );
                    dfd.resolve(result);

                } else {

                    PanelSwitcher().showLogin();
                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise()
        },

        doLogout: function() {

            const dfd = $.Deferred();

            Remote().logout()
            .then(result => {
                PanelSwitcher().showLogin();
                dfd.resolve(result);
            })
            .fail(jqxhr => {
                PanelSwitcher().showLogin();
                dfd.reject();
            });

            return dfd.promise()
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    function RegistrationForm( form_id )
    {
        //
        // Case when RegistrationForm is called as a function
        //
        if( ! ( this instanceof RegistrationForm ) )
        {
            return new RegistrationForm( form_id )
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        let _form_id = form_id || CONFIG.REGISTRATION_FORM_ID;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this,
        {
            form_id:
            {
                get: function() { return _form_id },
            },
        });
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( RegistrationForm.prototype,
    {

        clearForm: function() {

            $(`#${this.form_id} #registration-username`).val( '' );
            $(`#${this.form_id} #registration-email`).val( '' );
            $(`#${this.form_id} #registration-password`).val( '' );
            $(`#${this.form_id} #registration-password-repeat`).val( '' );
        },

        getFormFields: function() {

            let username = $(`#${this.form_id} #registration-username`).val();
            let email = $(`#${this.form_id} #registration-email`).val();
            let password = $(`#${this.form_id} #registration-password`).val();
            let password_repeat = $(`#${this.form_id} #registration-password-repeat`).val();

            let registration_data = {};

            registration_data['username'] = username || undefined;
            registration_data['email'] = email || undefined;
            registration_data['password'] = password || undefined;
            registration_data['password_repeat'] = password_repeat || undefined;

            return registration_data;
        },

        doRegister: function() {

            const self = this;

            let registration_data = this.getFormFields();

            const dfd = $.Deferred();

            if( registration_data.password != registration_data.password_repeat ) {
                PanelSwitcher().showMessage( 'Passwords must match' );
                dfd.reject();
                return;
            }

            Remote().register( registration_data )
            .done(function( result )
            {
                if( result.success ) {

                    self.clearForm();
                    dfd.resolve( result );

                } else {

                    PanelSwitcher().showMessage( result.message );
                    dfd.reject();
                }
            })
            .fail(function( xhr )
            {
                PanelSwitcher().showMessage( xhr.responseText );
                dfd.reject();
            });

            return dfd.promise();
        },

        toString: function()
        {
        },
    } );

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */
    /**
     * @author Ravendyne Inc / https://ravendyne.com
     */

    var REVISION = '1dev';

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    exports.Remote = Remote;
    exports.PanelSwitcher = PanelSwitcher;
    exports.LoginForm = LoginForm;
    exports.MealsForm = MealsForm;
    exports.UserForm = UserForm;
    exports.SettingsForm = SettingsForm;
    exports.RegistrationForm = RegistrationForm;
    exports.DOMTableClear = DOMTableClear;
    exports.DOMTableTemplateRowClone = DOMTableTemplateRowClone;
    exports.DOMTablePopulate = DOMTablePopulate;
    exports.DateLocalToISO = DateLocalToISO;
    exports.TimeLocalToISO = TimeLocalToISO;
    exports.DateISOToLocal = DateISOToLocal;
    exports.TimeISOToLocal = TimeISOToLocal;
    exports.REVISION = REVISION;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
