<?php?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Healthy Eating Habits</title>

        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" /> -->
        <link rel="stylesheet" href="./css/tempusdominus-bootstrap-4.min.css" />
        <link rel="stylesheet" href="./css/site.css">

        <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script> -->
        <script src="./js/jquery-3.3.1.min.js"></script>

    </head>
    <body>
    <main role="main">
        <div class="container">

        <div class="row">
        <div class="col-12">
            <?php include 'container_head.html'; ?>
        </div>
        </div> <!-- row -->

        <div class="row">
            <div class="col-12">
            <?php include 'top_card.html'; ?>
            </div>
        </div> <!-- row -->

        <div class="row">
        <div class="col-12">
            <div class="list-group list-group-horizontal d-none" id="list-tabs" role="tablist">
            <a class="list-group-item list-group-item-action" id="panel-register-list" data-toggle="list" href="#panel-register" role="tab" aria-controls="register">Register</a>
            <a class="list-group-item list-group-item-action" id="panel-login-list" data-toggle="list" href="#panel-login" role="tab" aria-controls="login">Login</a>
            <a class="list-group-item list-group-item-action" id="panel-members-list" data-toggle="list" href="#panel-members" role="tab" aria-controls="members">Members</a>
            <a class="list-group-item list-group-item-action" id="panel-manager-list" data-toggle="list" href="#panel-manager" role="tab" aria-controls="manager">Manager</a>
            <a class="list-group-item list-group-item-action" id="panel-admin-list" data-toggle="list" href="#panel-admin" role="tab" aria-controls="admin">Admin</a>
            <a class="list-group-item list-group-item-action active" id="panel-loading-list" data-toggle="list" href="#panel-loading" role="tab" aria-controls="loading">Loading</a>
            </div>
        </div>
        </div> <!-- row -->


        <div class="row mb-3">
        <div class="col-12">
            <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane" id="panel-register" role="tabpanel" aria-labelledby="panel-register-list">

            <?php include 'pane_register.html'; ?>

            </div> <!-- pane -->


            <div class="tab-pane" id="panel-login" role="tabpanel" aria-labelledby="panel-login-list">

            <?php include 'pane_login.html'; ?>

            </div> <!-- pane -->


            <div class="tab-pane" id="panel-members" role="tabpanel" aria-labelledby="panel-members-list">

            <?php include 'pane_members.html'; ?>

            </div> <!-- pane -->


            <div class="tab-pane" id="panel-manager" role="tabpanel" aria-labelledby="panel-manager-list">

            <?php include 'pane_manager.html'; ?>

            </div> <!-- pane -->


            <div class="tab-pane" id="panel-admin" role="tabpanel" aria-labelledby="panel-admin-list">

            <?php include 'pane_admin.html'; ?>

            </div> <!-- pane -->


            <div class="tab-pane show active" id="panel-loading" role="tabpanel" aria-labelledby="panel-loading-list">

            <?php include 'pane_loading.html'; ?>

            </div> <!-- pane -->


            </div> <!-- tab-content -->
        </div> <!-- col-12 -->
        </div> <!-- row -->

        <div class="row">
        <div class="col-12">
            <?php include 'bottom_card.html'; ?>
        </div>
        </div> <!-- row -->

        </div> <!-- containter -->
    </main>

    <?php include 'modal.html'; ?>

    </body>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>

    <!-- <script src="./js/moment-with-locales.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script> -->
    <script type="text/javascript" src="./js//moment.min.js"></script>
    <script type="text/javascript" src="./js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="./js/site.js"></script>
    <script src="./js/heh-app.js"></script>
    <script>
        $(document).ready(function(){
            HEH_on_inital_app_load();
        })
    </script>
</html>
