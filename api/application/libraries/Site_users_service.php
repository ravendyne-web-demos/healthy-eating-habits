<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_users_service {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    public function register_site_member( $request ) {

        // $group = array('members');
        $groups = array('2'); // must use ID

        return $this->register( $request, $groups );
    }

    public function register_site_manager( $request ) {

        // $group = array('managers');
        $groups = array('3'); // must use ID

        return $this->register( $request, $groups );
    }

    public function register( $request, $groups ) {

        $response = array( 'success' => false );

        $username = '';
        $password = '';
        $email = '';
        $additional_data = array(
            'first_name' => '',
            'last_name' => '',
            );

        if( array_key_exists( 'username', $request ) ) {
            $username = $request['username'];
        }
        if( array_key_exists( 'password', $request ) ) {
            $password = $request['password'];
        }
        if( array_key_exists( 'email', $request ) ) {
            $email = $request['email'];
        }

        if( ! ( $username && $password && $email ) ) {

            $response['message'] = 'missing required registration parameters';
            return $response;
        }

        if( $this->ion_auth->username_check( $username ) ) {

            $response['message'] = 'username already taken';
            return $response;
        }

        if( $this->ion_auth->email_check( $email ) ) {

            $response['message'] = 'email already taken';
            return $response;
        }

        $user_id = $this->ion_auth->register( $username, $password, $email, $additional_data, $groups );
        if( $user_id === false ) {
            $response['message'] = $this->ion_auth->errors();
            return $response;
        }

        $response['success'] = true;
        $response['user_id'] = $user_id;
        
        return $response;
    }

    public function update_site_user( $request ) {

        $response = array( 'success' => false );

        if( ! array_key_exists( 'id', $request ) ) {

            $response['message'] = 'missing id parameter';
            return $response;
        }

        $user_data = array();
        $user_id = intval( $request['id'] );

        if( array_key_exists( 'username', $request ) ) {
            $user_data['username'] = $request['username'];
        }
        if( array_key_exists( 'password', $request ) ) {
            $user_data['password'] = $request['password'];
        }
        if( array_key_exists( 'email', $request ) ) {
            $user_data['email'] = $request['email'];
        }

        if( array_key_exists( 'manager', $request ) ) {

            // there's only one 'admin' so a
            // user is either 'manager' or 'member'

            if( $request['manager'] === true ) {

                if( ! $this->ion_auth->in_group( '3', $user_id ) ) {

                    $this->ion_auth->add_to_group( '3', $user_id );
                    $this->ion_auth->remove_from_group( '2', $user_id );
                }

            } else {

                if( ! $this->ion_auth->in_group( '2', $user_id ) ) {

                    $this->ion_auth->add_to_group( '2', $user_id );
                    $this->ion_auth->remove_from_group( '3', $user_id );
                }
            }
        }

        if( ! $this->ion_auth->update( $user_id, $user_data ) ) {
            $response['message'] = $this->ion_auth->errors();
            return $response;
        }

        $response['success'] = true;
        
        return $response;
    }

    public function retrieve_user( $request ) {

        $response = array( 'success' => false );

        if( ! array_key_exists( 'id', $request ) ) {

            $response['message'] = 'missing id parameter';
            return $response;
        }

        $user_id = $request['id'];

        $user_groups = $this->ion_auth->get_users_groups( $user_id )->result();

        if( empty( $user_groups ) ) {

            $response['message'] = 'invalid user account';
            return $response;
        }

        // Get only group names as array
        $user_groups = array_map( function( $el ) { return $el->name; }, $user_groups );

        $user = $this->ion_auth->user( $user_id )->row();

        $response['success'] = true;
        $response['user'] = array(
            'id' => $user->user_id,
            'username' => $user->username,
            'email' => $user->email,
            'groups' => $user_groups,
            'is_admin' => $this->ion_auth->is_admin( $user_id ),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        );

        return $response;
    }

    public function retrieve_user_list( $request ) {

        $response = array( 'success' => false );

        // 2 -> 'members'
        $user_list = $this->ion_auth->users( '2' )->result();
        $user_list = array_map( function( $el ) {
            return array(
                'id' => $el->user_id,
                'username' => $el->username,
                'email' => $el->email,
                'active' => $el->active,
                'manager' => false
            );
        }, $user_list );

        // only admin can retrieve 'managers' too
        if( $this->ion_auth->is_admin() ) {

            // 3 -> 'managers'
            $managers_user_list = $this->ion_auth->users( '3' )->result();
            $managers_user_list = array_map( function( $el ) {
                return array(
                    'id' => $el->user_id,
                    'username' => $el->username,
                    'email' => $el->email,
                    'active' => $el->active,
                    'manager' => true
                );
            }, $managers_user_list );

            $user_list = array_merge( $user_list, $managers_user_list );
        }

        $user_list = array_filter( $user_list, function( $el ) {
            return $el['active'];
        } );
        // we have to do this here because array_filter() PRESERVES KEYS
        // which may result in $user_list being converted to JSON OBJECT {} instead of
        // JSON ARRAY [] when sent to API client.
        $user_list = array_values( $user_list );

        $response['success'] = true;
        $response['users'] = $user_list;
        
        return $response;
    }

    public function deactivate_user( $request ) {

        $response = array( 'success' => false );

        if( ! array_key_exists( 'id', $request ) ) {

            $response['message'] = 'missing id parameter';
            return $response;
        }

        $user_id = $request['id'];

        if( ! $this->ion_auth->update( $user_id, [ 'active' => '0' ] ) ) {
            $response['message'] = $this->ion_auth->errors();
            return $response;
        }

        $response['success'] = true;
        
        return $response;
    }

    public function is_member( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        return $this->ion_auth->in_group( '2', $id );
    }

    public function is_manager( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        return $this->ion_auth->in_group( '3', $id );
    }
}
