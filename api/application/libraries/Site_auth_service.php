<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_auth_service {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    //////////////////////////////////////////////////////////
    //
    // AUTHORIZATION
    //
    //////////////////////////////////////////////////////////

    public function check_access( $module ) {

        if( ! $this->ion_auth->logged_in() ) {

            return false;
        }

        if( $this->ion_auth->is_admin() ) {

            return true;
        }

        switch( $module ) {
            case 'meals':
                if( $this->site_users_service->is_member() )
                    return true;
            break;

            case 'users':
                if( $this->site_users_service->is_manager() )
                    return true;
            break;

            case 'settings':
                return true;
            break;
        }

        return false;
    }

    //////////////////////////////////////////////////////////
    //
    // AUTHENTICATION
    //
    //////////////////////////////////////////////////////////

    public function login( $request ) {

        $response = array( 'success' => false );

        $identity = '';
        $password = '';

        if( array_key_exists( 'identity', $request ) ) {
            $identity = $request['identity'];
        }
        if( array_key_exists( 'password', $request ) ) {
            $password = $request['password'];
        }

        if( ! ( $identity && $password ) ) {

            $response['message'] = 'missing required login parameters';
            return $response;
        }

        $remember = TRUE;

        if( ! $this->ion_auth->login($identity, $password, $remember) ) {

            $response['message'] = 'login attempt not successful';
            return $response;
        }

        $user = $this->ion_auth->user()->row();
        $user_r = $this->site_users_service->retrieve_user( array( 'id' => $user->user_id ) );

        if( ! $user_r['success'] ) {
            return $response;
        }


        $response['success'] = true;
        $response['user'] = $user_r['user'];

        return $response;
    }

    public function logout( $request ) {

        $response = array( 'success' => true );

        $this->ion_auth->logout();

        return $response;
    }
}
