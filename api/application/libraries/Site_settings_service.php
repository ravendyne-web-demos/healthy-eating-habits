<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class User_settings {
    public $total_daily_calories;
}

class Site_settings_service extends API_Controller {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    public function get_for_user( $request ) {

        $response = array( 'success' => false );

        $user = $this->ion_auth->user()->row();

        $settings_result = $this->get_settings_row( $user->id );

        if( empty( $settings_result ) ) {

            // insert default values for a user
            if( ! $this->create_default_settings( $user->id ) ) {

                $response['success'] = false;
                $response['message'] = 'error creating default user settings.';
            }

            $settings_result = $this->get_settings_row( $user->id );
        }

        $response['success'] = true;
        $response['data'] = $settings_result;

        return $response;
    }

    public function update_for_user( $request ) {

        $response = array( 'success' => false );

        $user = $this->ion_auth->user()->row();

        $settings_result = $this->get_settings_row( $user->id );

        if( empty( $settings_result ) ) {

            // insert default values for a user
            if( ! $this->create_default_settings( $user->id ) ) {

                $response['success'] = false;
                $response['message'] = 'error creating default user settings.';
            }
        }

        $data = [];

        if( array_key_exists( 'calories', $request ) ) {
            $data['calories'] = $request['calories'];
        }

        $this->db->reset_query();
        $this->db->where( 'user_id', $user->id );
        $this->db->update('settings', $data);

        $response['success'] = $this->db->affected_rows() > 0;

        return $response;
    }

    private function create_default_settings( $user_id ) {

        $default_settings = [
            'calories' => '1800',
            'user_id' => $user_id
        ];

        $this->db->reset_query();

        return $this->db->insert( 'settings', $default_settings );
    }

    private function convert_settings( $data_row ) {

        $conversion = [];

        if( is_object( $data_row ) ) {

            $conversion = [
                'calories' => $data_row->calories
            ];

        } else if( is_array( $data_row ) && ! empty( $data_row ) ) {

            $conversion = [
                'calories' => $data_row['calories']
            ];
        }

        return $conversion;
    }

    private function get_settings_row( $user_id ) {

        $this->db->reset_query();
        $this->db->where( 'user_id', $user_id );
        $settings_result = $this->db->get('settings')->result_array();

        if( ! empty( $settings_result ) ) {
            $settings_result = $settings_result[0];
        }

        return $this->convert_settings( $settings_result );
    }
}
