<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_meals_service {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    public function create_meal( $request ) {

        $response = array( 'success' => false );

        $mdate = date( 'Y-m-d' ); // YYYY-MM-DD
        $mtime = date( 'H:i:s' ); // HH:mm:ss
        $name = '[name not set]';
        $calories = 0;

        if( array_key_exists( 'mdate', $request ) ) {
            $mdate = $request['mdate'];
        }

        if( array_key_exists( 'mtime', $request ) ) {
            $mtime = $request['mtime'];
        }

        if( array_key_exists( 'name', $request ) ) {
            $name = $request['name'];
        }

        if( array_key_exists( 'calories', $request ) ) {
            $calories = intval( $request['calories'] );
        }

        if( ! strtotime( $request['mdate'] . ' ' . $request['mtime'] ) ) {

            $response['message'] = 'Invalid date/time parameters';
            return $response;
        }

        $user_id = null;

        if( $this->ion_auth->is_admin() && array_key_exists( 'user_id', $request ) ) {

            // admin gets to retrieve for anyone
            $user_id = intval( $request['user_id'] );

        } else {

            $user = $this->ion_auth->user()->row();
            $user_id = $user->user_id;
        }

        $data = array(
            'mdate' => $mdate,
            'mtime' => $mtime,
            'name' => $name,
            'calories' => $calories,
            'user_id' => $user_id
        );

        $insert_success = $this->db->insert( 'meals', $data );

        $response['success'] = $insert_success;
        $response['data'] = $data;

		return $response;
    }

    public function retrieve_meals( $request ) {

        $user = $this->ion_auth->user()->row();

        $filter = array();

        if( array_key_exists( 'filter', $request ) ) {

            $request_filter = $request['filter'];

            if( array_key_exists( 'date_from', $request_filter ) ) {
                $filter['mdate >='] = $request_filter['date_from'];
            }
            if( array_key_exists( 'date_to', $request_filter ) ) {
                $filter['mdate <='] = $request_filter['date_to'];
            }
            if( array_key_exists( 'time_from', $request_filter ) ) {
                $filter['mtime >='] = $request_filter['time_from'];
            }
            if( array_key_exists( 'time_to', $request_filter ) ) {
                $filter['mtime <='] = $request_filter['time_to'];
            }
        }

        $this->db->select( 'id, mdate, mtime, name, calories' );
        if( ! empty( $filter ) ) {
            $this->db->where( $filter );
        }

        $user_id = $user->user_id;
        if( $this->ion_auth->is_admin() && array_key_exists( 'user_id', $request ) ) {
            // admin gets to retrieve for anyone
            $user_id = intval( $request['user_id'] );
        }
        $this->db->where( 'user_id', $user_id );

        $query = $this->db->get( 'meals' );

        $meals = array();
        foreach( $query->result() as $row )
        {
            $meals[] = $row;
        }

        $response = array( 'success' => true );
        $response['meals'] = $this->postprocess( $meals, $user_id, $filter );
        $response['filter'] = $filter;

		return $response;
    }

    private function postprocess( $meals, $user_id, $filter ) {

        $settings_r = $this->site_settings_service->get_for_user([]);
        if( ! $settings_r['success'] ) {
            return $meals;
        }

        $max_daily_calories = $settings_r['data']['calories'];

        $totals_filter = [];
        if( array_key_exists( 'date_from', $filter ) ) {
            $totals_filter['mdate >='] = $filter['date_from'];
        }
        if( array_key_exists( 'date_to', $filter ) ) {
            $totals_filter['mdate <='] = $filter['date_to'];
        }

        $this->db->reset_query();
        $this->db->where( 'user_id', $user_id );
        if( ! empty( $totals_filter ) ) {
            $this->db->where( $totals_filter );
        }
        $daily_totals_qr = $this->db->get('meal_totals')->result_array();

        $daily_totals = [];
        foreach( $daily_totals_qr as $dtr ) {
            $daily_totals[ $dtr['mdate'] ] = $dtr['daily_total'];
        }

        $meals = array_map( function( $el ) use( $max_daily_calories, $daily_totals ) {
            $daily_total = $daily_totals[ $el->mdate ];
            if( $daily_total > $max_daily_calories ) {
                $el->exceeded = true;
            } else {
                $el->exceeded = false;
            }
            return $el;
        }, $meals );

        return $meals;
    }

    public function update_meal( $request ) {

        $response = array( 'success' => false );

        if( ! array_key_exists( 'id', $request ) ) {

            $response['message'] = 'Missing id parameter';
            return $response;
        }

        $id = intval( $request['id'] );
        $data = array();
        $datetime_value = '';

        if( array_key_exists( 'mdate', $request ) ) {
            $data['mdate'] = $request['mdate'];
            $datetime_value = $request['mdate'];
        }

        if( array_key_exists( 'mtime', $request ) ) {
            $data['mtime'] = $request['mtime'];
            $datetime_value .= ' ' . $request['mtime'];
        }

        if( array_key_exists( 'name', $request ) ) {
            $data['name'] = $request['name'];
        }

        if( array_key_exists( 'calories', $request ) ) {
            $data['calories'] = intval( $request['calories'] );
        }

        if( $datetime_value && ! strtotime( $datetime_value ) ) {

            $response['message'] = 'Invalid date/time parameters';
            return $response;
        }

        // only admin gets to retrieve for anyone
        if( ! $this->ion_auth->is_admin() ) {

            $user = $this->ion_auth->user()->row();

            $this->db->where( 'user_id', $user->user_id );
        }

        $this->db->where( 'id', $id );
        $this->db->update( 'meals', $data );

        $response['success'] = $this->db->affected_rows() > 0;
        $response['data'] = $data;

		return $response;
    }

    public function delete_meal( $request ) {

        $response = array( 'success' => false );

        if( ! array_key_exists( 'id', $request ) ) {

            $response['message'] = 'Missing id parameter';
            return $response;
        }

        $id = intval( $request['id'] );

        // only admin gets to delete for anyone
        if( ! $this->ion_auth->is_admin() ) {

            $user = $this->ion_auth->user()->row();

            $this->db->where( 'user_id', $user->user_id );
        }

        $this->db->where( 'id', $id );
        $this->db->delete( 'meals' );

        $response['success'] = $this->db->affected_rows() > 0;
        $response['request'] = $request;

		return $response;
    }
}
