<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

// (a kind of) JSON API style base controller
class API_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    protected function parse_request( $allow_empty = false ) {

        $request = json_decode($this->input->raw_input_stream, true);

        $json_conversion_error = $this->decode_json_error();

        if( $json_conversion_error ) {

            $response = array( 'success' => false, 'message' => $json_conversion_error );

            header('Content-Type: application/json');
			echo json_encode( $response );

            return false;
        }

        if( ! $allow_empty && empty( $request ) ) {

            $response = array( 'success' => false, 'message' => 'empty request' );

            header('Content-Type: application/json');
			echo json_encode( $response );

            return false;
        }
        
        return $request;
    }

    protected function send_response( $response ) {

        header('Content-Type: application/json');
		echo json_encode( $response );
    }

    protected function decode_json_error() {

        switch( json_last_error() ) {
            case JSON_ERROR_NONE:
                return '';
            break;
            case JSON_ERROR_DEPTH:
                return 'The maximum stack depth has been exceeded ';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                return 'Invalid or malformed JSON';
            break;
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                return 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            case JSON_ERROR_RECURSION:
                return 'One or more recursive references in the value to be encoded';
            break;
            case JSON_ERROR_INF_OR_NAN:
                return 'One or more NAN or INF values in the value to be encoded';
            break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                return 'A value of a type that cannot be encoded was given';
            break;
            case JSON_ERROR_INVALID_PROPERTY_NAME:
                return 'A property name that cannot be encoded was given';
            break;
            case JSON_ERROR_UTF16:
                return 'Malformed UTF-16 characters, possibly incorrectly encoded';
            break;
		}
		
		return 'Unknown error';
    }
}
