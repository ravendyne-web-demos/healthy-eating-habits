<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

// (a kind of) REST API style base controller
class API_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    // public function _remap( $method, $params = array() )
    // {
    //     // $method = 'process_'.$method;
    //     if (method_exists($this, $method))
    //     {
    //         return call_user_func_array(array($this, $method), $params);
    //     }
    //     show_404();
    // }

    public function _remap( $first_segment, $other_segments = array() )
    {
        return $this->index( $first_segment, $other_segments );
    }

    public function index( $first_segment, $other_segments ) {

        $method = $this->input->method(true);

        switch($method) {
            case 'GET': return $this->get( $first_segment, $other_segments );
            case 'PUT': return $this->put( $first_segment, $other_segments );
            case 'POST': return $this->post( $first_segment, $other_segments );
            case 'DELETE': return $this->delete( $first_segment, $other_segments );
        }

        // TODO replace with 404 response
        show_404();
    }

    protected function get( $first_segment, $other_segments ) {
        echo 'get wha...?!';
    }

    protected function post( $first_segment, $other_segments ) {
        echo 'post wha...?!';
    }

    protected function put( $first_segment, $other_segments ) {
        echo 'put wha...?!';
    }

    protected function delete( $first_segment, $other_segments ) {
        echo 'delete wha...?!';
    }
}
