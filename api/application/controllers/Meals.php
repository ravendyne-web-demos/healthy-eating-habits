<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Meals extends API_Controller {

    public function index()
    {
        $this->retrieve();
    }

    public function create()
    {
        if( ! $this->site_auth_service->check_access( 'meals' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
        if( $request === false ) return;

		$response = $this->site_meals_service->create_meal( $request );

		$this->send_response( $response );
    }

    public function retrieve()
    {
        if( ! $this->site_auth_service->check_access( 'meals' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $response = $this->site_meals_service->retrieve_meals( $request );

		$this->send_response( $response );
    }

    public function update()
    {
        if( ! $this->site_auth_service->check_access( 'meals' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

        $response = $this->site_meals_service->update_meal( $request );

		$this->send_response( $response );
    }

    public function delete()
    {
        if( ! $this->site_auth_service->check_access( 'meals' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

        $response = $this->site_meals_service->delete_meal( $request );

		$this->send_response( $response );
    }
}
