<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends API_Controller {

    public function index()
    {
        $this->retrieve();
    }

	public function update() {

        if( ! $this->site_auth_service->check_access( 'settings' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = $this->site_settings_service->update_for_user( $request );

		$this->send_response( $response );
	}

    public function retrieve()
    {
        if( ! $this->site_auth_service->check_access( 'settings' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $response = $this->site_settings_service->get_for_user( $request );

		$this->send_response( $response );
    }
}
