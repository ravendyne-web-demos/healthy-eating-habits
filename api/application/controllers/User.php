<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends API_Controller {

    public function index()
    {
        $this->retrieve();
    }

	public function create() {

        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = array( 'success' => false );

		if( array_key_exists( 'manager', $request ) && $request['manager'] === true ) {
			$response = $this->site_users_service->register_site_manager( $request );
		} else {
			$response = $this->site_users_service->register_site_member( $request );
		}

		$this->send_response( $response );
	}

	public function update() {

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = $this->site_users_service->update_site_user( $request );

		$this->send_response( $response );
	}



    public function retrieve()
    {
        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $response = $this->site_users_service->retrieve_user( $request );

		$this->send_response( $response );
    }

    public function list()
    {
        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $response = $this->site_users_service->retrieve_user_list( $request );

		$this->send_response( $response );
    }

    public function delete()
    {
        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

        $response = $this->site_users_service->deactivate_user( $request );

		$this->send_response( $response );
    }
}
