<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends API_Controller {

	public function index()
	{
		$this->login();
	}

	public function who_am_i() {

		if( ! $this->ion_auth->logged_in() ) {
			$this->send_response( array( 'success' => false, 'needs_login' => true, 'message' => 'not authorized' ) );
			return;
		}

		$request = $this->parse_request( true );
		if( $request === false ) return;

		$response = array( 'success' => false );

		$user = $this->ion_auth->user()->row();
        $response = $this->site_users_service->retrieve_user( array( 'id' => $user->user_id ) );

		$this->send_response( $response );
	}

	public function register() {

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = array( 'success' => false );

		$response = $this->site_users_service->register_site_member( $request );

		$this->send_response( $response );
	}

	public function login()
	{
		$request = $this->parse_request();
		if( $request === false ) return;

		$response = $this->site_auth_service->login( $request );

		$this->send_response( $response );
	}

	public function logout()
	{
		$request = $this->parse_request( true );
		if( $request === false ) return;

		$response = $this->site_auth_service->logout( $request );

		$this->send_response( $response );
	}
}
