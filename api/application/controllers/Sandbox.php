<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandbox extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$arr = array('a' => 1, 'APPPATH' => APPPATH, 'tables' => array());

		$tables = $this->db->list_tables();
		foreach ($tables as $table)
		{
				$arr['tables'][] = $table;
		}

		if( ! $this->ion_auth->logged_in() ) {
			$arr['logged_in'] = false;
		} else {
			$arr['logged_in'] = true;
		}
		// $query = $this->db->get('test_tbl');
		// foreach( $query->result() as $row )
		// {
		// 	$arr['test_tbl_row'] = $row->name;
		// }

		$jsonArray = json_decode($this->input->raw_input_stream, true);
		if( ! $jsonArray ) {
			$jsonArray = array();
		}
		if( array_key_exists( 'command', $jsonArray ) ) {
			$arr['response'] = $jsonArray['command'];
		}
		header('Content-Type: application/json');
		echo json_encode( $arr );
	}
}
